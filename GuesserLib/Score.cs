﻿namespace Guesser
{
    public class Score
    {
        public string Title
        {
            get;
            set;
        }

        public string Caption
        {
            get;
            set;
        }

        public string Max
        {
            get;
            set;
        }

        public string Min
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Guesser
{
    public class ConfigContent
    {
        public string Path
        {
            get;
            set;
        }

        private List<Thing> things = null;
        public List<Thing> Things
        {
            get 
            {
                if (things == null) Read();
                return things;
            }
        }

        public ConfigContent()
        {

        }

        public void Read()
        {
            List<Thing> thingsList= new List<Thing>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(Path);
            XmlElement root = xDoc.DocumentElement;
            if (root != null)
            {
                foreach (XmlElement thingItem in root)
                {
                    if (thingItem.Name == "thing")
                    {
                        Thing thing = new Thing();
                        thingsList.Add(thing);
                        thing.Name = thingItem.Attributes.GetNamedItem("name").Value;

                        foreach (XmlElement sectionThingItem in thingItem)
                        {
                            if (sectionThingItem.Name == "question")
                            {
                                foreach (XmlElement questionThingItem in sectionThingItem)
                                {
                                    if (questionThingItem.Name == "image") thing.QuestionPathImage = GetAbsolutePath(questionThingItem.InnerText);
                                    if (questionThingItem.Name == "background")
                                        thing.QuestionBackgroundPathImage = GetAbsolutePath(questionThingItem.InnerText);
                                    if (questionThingItem.Name == "answer1") thing.Answer1 = questionThingItem.InnerText;
                                    if (questionThingItem.Name == "answer2") thing.Answer2 = questionThingItem.InnerText;
                                    if (questionThingItem.Name == "answer3") thing.Answer3 = questionThingItem.InnerText;
                                    if (questionThingItem.Name == "answer4") thing.Answer4 = questionThingItem.InnerText;
                                    if (questionThingItem.Name == "help") thing.Help = questionThingItem.InnerText;
                                }
                            }
                            if (sectionThingItem.Name == "description")
                            {
                                foreach (XmlElement descriptionThingItem in sectionThingItem)
                                {
                                    if (descriptionThingItem.Name == "image") thing.DescriptionPathImage = GetAbsolutePath(descriptionThingItem.InnerText);
                                    if (descriptionThingItem.Name == "background") thing.DescriptionBackgroundPathImage = GetAbsolutePath(descriptionThingItem.InnerText);
                                    if (descriptionThingItem.Name == "text") thing.Description = descriptionThingItem.InnerText;
                                }
                            }
                        }
                    }
                }
            }
            things = thingsList;
        }

        public void Write()
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode rootNode = xDoc.CreateElement("content");
            xDoc.AppendChild(rootNode);

            foreach (Thing item in things)
            {
                XmlNode thingNode = xDoc.CreateElement("thing");
                XmlAttribute thingAttribute = xDoc.CreateAttribute("name");
                thingAttribute.Value = item.Name;
                thingNode.Attributes.Append(thingAttribute);

                XmlNode questionNode = xDoc.CreateElement("question");
                thingNode.AppendChild(questionNode);
                XmlNode questionBackgroundNode = xDoc.CreateElement("background");
                questionBackgroundNode.InnerText = item.QuestionBackgroundPathImage;
                questionNode.AppendChild(questionBackgroundNode);
                XmlNode questionImageNode = xDoc.CreateElement("image");
                questionImageNode.InnerText = item.QuestionPathImage;
                questionNode.AppendChild(questionImageNode);

                XmlNode questionAnswer1Node = xDoc.CreateElement("answer1");
                questionAnswer1Node.InnerText = item.Answer1;
                questionNode.AppendChild(questionAnswer1Node);
                XmlNode questionAnswer2Node = xDoc.CreateElement("answer2");
                questionAnswer2Node.InnerText = item.Answer2;
                questionNode.AppendChild(questionAnswer2Node);
                XmlNode questionAnswer3Node = xDoc.CreateElement("answer3");
                questionAnswer3Node.InnerText = item.Answer3;
                questionNode.AppendChild(questionAnswer3Node);
                XmlNode questionAnswer4Node = xDoc.CreateElement("answer4");
                questionAnswer4Node.InnerText = item.Answer4;
                questionNode.AppendChild(questionAnswer4Node);
                XmlNode questionHelpNode = xDoc.CreateElement("help");
                questionHelpNode.InnerText = item.Help;
                questionNode.AppendChild(questionHelpNode);


                XmlNode descriptionNode = xDoc.CreateElement("description");
                thingNode.AppendChild(descriptionNode);
                XmlNode descriptionBackgroundNode = xDoc.CreateElement("background");
                descriptionBackgroundNode.InnerText = item.DescriptionBackgroundPathImage;
                descriptionNode.AppendChild(descriptionBackgroundNode);
                XmlNode descriptionImageNode = xDoc.CreateElement("image");
                descriptionImageNode.InnerText = item.DescriptionPathImage;
                descriptionNode.AppendChild(descriptionImageNode);
                XmlNode descriptionTextNode = xDoc.CreateElement("text");
                descriptionTextNode.InnerText = item.Description;
                descriptionNode.AppendChild(descriptionTextNode);

                rootNode.AppendChild(thingNode);
            }

            xDoc.Save(Path);
        }

        private string GetAbsolutePath(string relativePath)
        {
            if (relativePath.Length > 3 && 
                relativePath.Substring(0, 3) == @"..\")
            {
                return (new FileInfo(Path)).DirectoryName + relativePath.Substring(2);
            }
            return relativePath;
        }

        public static void WriteEmpty(string path)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode rootNode = xDoc.CreateElement("content");
            xDoc.AppendChild(rootNode);

            XmlNode thingNode = xDoc.CreateElement("thing");
            XmlAttribute thingAttribute = xDoc.CreateAttribute("name");
            thingNode.Attributes.Append(thingAttribute);

            XmlNode questionNode = xDoc.CreateElement("question");
            thingNode.AppendChild(questionNode);
            XmlNode questionBackgroundNode = xDoc.CreateElement("background");
            questionNode.AppendChild(questionBackgroundNode);
            XmlNode questionImageNode = xDoc.CreateElement("image");
            questionNode.AppendChild(questionImageNode);

            XmlNode questionAnswer1Node = xDoc.CreateElement("answer1");
            questionNode.AppendChild(questionAnswer1Node);
            XmlNode questionAnswer2Node = xDoc.CreateElement("answer2");
            questionNode.AppendChild(questionAnswer2Node);
            XmlNode questionAnswer3Node = xDoc.CreateElement("answer3");
            questionNode.AppendChild(questionAnswer3Node);
            XmlNode questionAnswer4Node = xDoc.CreateElement("answer4");
            questionNode.AppendChild(questionAnswer4Node);
            XmlNode questionHelpNode = xDoc.CreateElement("help");
            questionNode.AppendChild(questionHelpNode);


            XmlNode descriptionNode = xDoc.CreateElement("description");
            thingNode.AppendChild(descriptionNode);
            XmlNode descriptionBackgroundNode = xDoc.CreateElement("background");
            descriptionNode.AppendChild(descriptionBackgroundNode);
            XmlNode descriptionImageNode = xDoc.CreateElement("image");
            descriptionNode.AppendChild(descriptionImageNode);
            XmlNode descriptionTextNode = xDoc.CreateElement("text");
            descriptionNode.AppendChild(descriptionTextNode);

            rootNode.AppendChild(thingNode);

            xDoc.Save(path);
        }
    }
}

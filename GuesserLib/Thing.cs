﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guesser
{
    public class Thing
    {
        public delegate void ThingEvent (object sender, string oldValue, string newValue);
        public event ThingEvent ChangeName;

        private string name;
        public string Name
        {
            get => name;
            set
            {
                if (ChangeName != null) ChangeName(this, name, value);
                name = value;
            }
        }

        public string Answer1
        {
            get;
            set;
        }

        public string Answer2
        {
            get;
            set;
        }

        public string Answer3
        {
            get;
            set;
        }

        public string Answer4
        {
            get;
            set;
        }

        public string Help
        {
            get;
            set;
        }

        public string QuestionPathImage
        {
            get;
            set;
        }

        public string QuestionBackgroundPathImage
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string DescriptionPathImage
        {
            get;
            set;
        }

        public string DescriptionBackgroundPathImage
        {
            get;
            set;
        }
    }
}

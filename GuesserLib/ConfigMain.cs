﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Guesser
{
    public class ConfigMain
    {
        public string Path
        {
            get;
            set;
        }

        private MainData data = null;
        public MainData Data
        {
            get
            {
                if (data == null) Read();
                return data;
            }
        }

        public void Read()
        {
            data = new MainData();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(Path);
            XmlElement root = xDoc.DocumentElement;
            if (root != null)
            {
                foreach (XmlElement item in root)
                {
                    if (item.Name == "content") data.Content = item.InnerText;
                    if (item.Name == "maxCountQuestions") data.MaxCountQuestions = item.InnerText;
                    if (item.Name == "timerToStart") data.TimerToStart = item.InnerText;
                    if (item.Name == "exitSpeech") data.ExitSpeech = item.InnerText;
                    if (item.Name == "confidence") data.Confidence = item.InnerText;
                    if (item.Name == "backgroundStart") data.BackgroundStart = item.InnerText;
                    if (item.Name == "backgroundFinish") data.BackgroundFinish = item.InnerText;
                    if (item.Name == "backgroundStat") data.BackgroundStat = item.InnerText;

                    data.Scores = new List<Score>();
                    if (item.Name == "scores")
                    {
                        foreach (XmlElement scoreItem in item)
                        {
                            Score score = new Score();
                            foreach (XmlElement scoreParamItem in scoreItem)
                            {
                                if (scoreParamItem.Name == "title") score.Title = scoreParamItem.InnerText;
                                if (scoreParamItem.Name == "caption") score.Caption = scoreParamItem.InnerText;
                                if (scoreParamItem.Name == "max") score.Max = scoreParamItem.InnerText;
                                if (scoreParamItem.Name == "min") score.Min = scoreParamItem.InnerText;
                            }
                            data.Scores.Add(score);
                        }
                    }
                }
            }
        }

        public void Write()
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode rootNode = xDoc.CreateElement("main");
            xDoc.AppendChild(rootNode);

            XmlNode contentNode = xDoc.CreateElement("content");
            contentNode.InnerText = data.Content;
            rootNode.AppendChild(contentNode);
            
            XmlNode maxCountQuestionsNode = xDoc.CreateElement("maxCountQuestions");
            maxCountQuestionsNode.InnerText = data.MaxCountQuestions;
            rootNode.AppendChild(maxCountQuestionsNode);

            XmlNode timerToStartNode = xDoc.CreateElement("timerToStart");
            timerToStartNode.InnerText = data.TimerToStart;
            rootNode.AppendChild(timerToStartNode);

            XmlNode exitSpeechNode = xDoc.CreateElement("exitSpeech");
            exitSpeechNode.InnerText = data.ExitSpeech;
            rootNode.AppendChild(exitSpeechNode);

            XmlNode confidenceNode = xDoc.CreateElement("confidence");
            confidenceNode.InnerText = data.Confidence;
            rootNode.AppendChild(confidenceNode);

            XmlNode backgroundStartNode = xDoc.CreateElement("backgroundStart");
            backgroundStartNode.InnerText = data.BackgroundStart;
            rootNode.AppendChild(backgroundStartNode);

            XmlNode backgroundFinishNode = xDoc.CreateElement("backgroundFinish");
            backgroundFinishNode.InnerText = data.BackgroundFinish;
            rootNode.AppendChild(backgroundFinishNode);

            XmlNode backgroundStatNode = xDoc.CreateElement("backgroundStat");
            backgroundStatNode.InnerText = data.BackgroundStat;
            rootNode.AppendChild(backgroundStatNode);

            XmlNode scoresNode = xDoc.CreateElement("scores");
            rootNode.AppendChild(scoresNode);

            foreach (Score scoreItem in data.Scores)
            {
                XmlNode scoreNode = xDoc.CreateElement("score");

                XmlNode scoreTitleNode = xDoc.CreateElement("title");
                scoreTitleNode.InnerText = scoreItem.Title;
                XmlNode scoreCaptionNode = xDoc.CreateElement("caption");
                scoreCaptionNode.InnerText = scoreItem.Caption;
                XmlNode scoreMaxNode = xDoc.CreateElement("max");
                scoreMaxNode.InnerText = scoreItem.Max;
                XmlNode scoreMinNode = xDoc.CreateElement("min");
                scoreMinNode.InnerText = scoreItem.Min;

                scoreNode.AppendChild(scoreTitleNode);
                scoreNode.AppendChild(scoreCaptionNode);
                scoreNode.AppendChild(scoreMaxNode);
                scoreNode.AppendChild(scoreMinNode);

                scoresNode.AppendChild(scoreNode);
            }

            xDoc.Save(Path);
        }

        public static void WriteEmpty(string path)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode rootNode = xDoc.CreateElement("main");
            xDoc.AppendChild(rootNode);

            XmlNode contentNode = xDoc.CreateElement("content");
            rootNode.AppendChild(contentNode);

            XmlNode maxCountQuestionsNode = xDoc.CreateElement("maxCountQuestions");
            rootNode.AppendChild(maxCountQuestionsNode);

            XmlNode timerToStartNode = xDoc.CreateElement("timerToStart");
            rootNode.AppendChild(timerToStartNode);

            XmlNode exitSpeechNode = xDoc.CreateElement("exitSpeech");
            rootNode.AppendChild(exitSpeechNode);

            XmlNode confidenceNode = xDoc.CreateElement("confidence");
            rootNode.AppendChild(confidenceNode);

            XmlNode backgroundStartNode = xDoc.CreateElement("backgroundStart");
            rootNode.AppendChild(backgroundStartNode);

            XmlNode backgroundFinishNode = xDoc.CreateElement("backgroundFinish");
            rootNode.AppendChild(backgroundFinishNode);

            XmlNode backgroundStatNode = xDoc.CreateElement("backgroundStat");
            rootNode.AppendChild(backgroundStatNode);

            XmlNode scoresNode = xDoc.CreateElement("scores");
            rootNode.AppendChild(scoresNode);

            xDoc.Save(path);
        }
    }
}

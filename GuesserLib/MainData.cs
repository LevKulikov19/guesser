﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guesser
{
    public class MainData
    {
        public string Content
        {
            get;
            set;
        }

        public string MaxCountQuestions
        {
            get;
            set;
        }

        public string TimerToStart
        {
            get;
            set;
        }

        public string ExitSpeech
        {
            get;
            set;
        }

        public string Confidence
        {
            get;
            set;
        }

        public string BackgroundStart
        {
            get;
            set;
        }

        public string BackgroundFinish
        {
            get;
            set;
        }

        public string BackgroundStat
        {
            get;
            set;
        }

        public List<Score> Scores
        {
            get;
            set;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guesser
{
    public partial class FinishControl : UserControl, ISpeakFinishControl
    {
        public delegate void FinishControlEvent(object sender);
        public event FinishControlEvent Stat;
        public event FinishControlEvent NewGame;

        private List<Score> scores;
        public List<Score> Scores
        {
            get => scores;
            set
            {
                scores = value;
                RefreshLableTitle();
            }
        }



        private Dictionary<Thing, bool> thingQuestion;
        public Dictionary<Thing, bool> ThingQuestion
        {
            get => thingQuestion;
            set
            {
                thingQuestion = value;
                RefreshLableResult();
                RefreshLableTitle();
            }
        }

        public Image ImageBackground
        {
            get => imageBG;
            set
            {
                imageBG = value;
            }
        }

        Image imageBG = ResourceMain.BackgroundQuality;
        Image imageMic = ResourceMain.Mic;

        public FinishControl()
        {
            InitializeComponent();
        }

        private void RefreshLableTitle()
        {
            if (ThingQuestion == null) return;
            if (Scores == null) return;
            int success = getSuccessAnswer(ThingQuestion);
            foreach (Score item in Scores)
            {
                int min, max;
                try
                {
                    min = int.Parse(item.Min);
                }
                catch (Exception)
                {
                    MessageBox.Show("В основном конфигурационном файле не корректно указан параметр min в spore");
                    continue;
                }
                try
                {
                    max = int.Parse(item.Max);
                }
                catch (Exception)
                {
                    MessageBox.Show("В основном конфигурационном файле не корректно указан параметр max в spore");
                    continue;
                }
                if (success >= min && success <= max)
                {
                    LabelTitle.Text = item.Title;
                    LabelCaption.Text = item.Caption;
                    break;
                }
            }
        }

        private void RefreshLableResult()
        {
            LabelResult.Text = "Вы набрали " + getSuccessAnswer(ThingQuestion) + " / " + ThingQuestion.Count().ToString();
            Refresh();
        }

        private int getSuccessAnswer (Dictionary<Thing, bool> thingQuestion)
        {
            var successAnswer = from thing in thingQuestion
                                where thing.Value.Equals(true)
                                select thing;
            return successAnswer.Count();
        }

        private void FinishControl_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void Draw()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gPaint.DrawImage(imageBG, ClientRectangle);

            LabelStat.DrawText(gPaint, LabelStat.Location);

            LabelNewGame.DrawText(gPaint, LabelNewGame.Location);

            LabelSpeek.DrawText(gPaint, LabelSpeek.Location);
            Point pointMic = LabelSpeek.Location;
            pointMic.Offset(-55, -10);
            Rectangle rectangleMic = new Rectangle(pointMic, imageMic.Size);
            gPaint.DrawImage(imageMic, rectangleMic);

            LabelTitle.DrawText(gPaint, LabelTitle.Location);

            LabelCaption.DrawText(gPaint, LabelCaption.Location);

            LabelResult.DrawText(gPaint, LabelResult.Location);

            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void LabelStat_Click(object sender, EventArgs e)
        {
            Stat(this);
        }

        private void LabelNewGame_Click(object sender, EventArgs e)
        {
            NewGame(this);
        }

        public void ClickStat()
        {
            LabelStat_Click(this, null);
        }

        public void ClickNewGame()
        {
            LabelNewGame_Click(this, null);
        }


    }
}

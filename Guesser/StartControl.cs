﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace Guesser
{
    public partial class StartControl : UserControl, ISpeakStartControl
    {
        public delegate void StartControlEvent (object sender);
        public event StartControlEvent Start;
        public event StartControlEvent Exit;

        public Image ImageBackground
        {
            get => imageBG;
            set
            {
                imageBG = value;
            }
        }

        Image imageBG = ResourceMain.BackgroundStart;
        Image imageMic = ResourceMain.Mic;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font mainFont;

        public StartControl()
        {
            byte[] fontData = ResourceMain.KTFJermilov_Solid;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, ResourceMain.KTFJermilov_Solid.Length);
            AddFontMemResourceEx(fontPtr, (uint)ResourceMain.KTFJermilov_Solid.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            mainFont = new Font(fonts.Families[0], 16.0F);

            InitializeComponent();

            foreach (Control item in this.Controls)
            {
                if (item is TransparentLabel)
                {
                    float size = (item as TransparentLabel).Font.Size;
                    (item as TransparentLabel).Font = new Font(mainFont.FontFamily, size);
                }
            }
        }

        private void LabelStart_Click(object sender, EventArgs e)
        {
            if (Start != null) Start(this);
        }

        private void Draw()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            gPaint.DrawImage(imageBG, ClientRectangle);

            LabelSpeek.DrawText(gPaint, LabelSpeek.Location);
            Point pointMic = LabelSpeek.Location;
            pointMic.Offset(-55, -10);
            Rectangle rectangleMic = new Rectangle(pointMic, imageMic.Size);
            gPaint.DrawImage(imageMic, rectangleMic);

            LabelExit.DrawText(gPaint, LabelExit.Location);
            LabelGameName.DrawText(gPaint, LabelGameName.Location);
            LabelLogo.DrawText(gPaint, LabelLogo.Location);
            LabelStart.DrawText(gPaint, LabelStart.Location);

            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void StartControl_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void LabelExit_Click(object sender, EventArgs e)
        {
            if (Exit != null) Exit(this);
        }

        public void ClickStart()
        {
            LabelStart_Click(null, null);
        }

        public void ClickExit()
        {
            LabelExit_Click(null, null);
        }

        private void StartControl_Load(object sender, EventArgs e)
        {

        }

        private void LabelGameName_Click(object sender, EventArgs e)
        {

        }

        private void LabelLogo_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guesser
{
    interface ISpeakQuestionControl
    {
        void ClickAnswer1();
        void ClickAnswer2();
        void ClickAnswer3();
        void ClickAnswer4();
        void ClickExit();
        void ClickSkip();
    }
}

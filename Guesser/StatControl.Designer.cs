﻿
namespace Guesser
{
    partial class StatControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelStat = new Guesser.TransparentLabel();
            this.LabelResult = new Guesser.TransparentLabel();
            this.LabelSpeek = new Guesser.TransparentLabel();
            this.LabelNewGame = new Guesser.TransparentLabel();
            this.item = new Guesser.TransparentLabel();
            this.LabelThing2 = new Guesser.TransparentLabel();
            this.LabelThing3 = new Guesser.TransparentLabel();
            this.LabelThing5 = new Guesser.TransparentLabel();
            this.LabelThing7 = new Guesser.TransparentLabel();
            this.LabelThing9 = new Guesser.TransparentLabel();
            this.LabelThing4 = new Guesser.TransparentLabel();
            this.LabelThing6 = new Guesser.TransparentLabel();
            this.LabelThing8 = new Guesser.TransparentLabel();
            this.LabelThing10 = new Guesser.TransparentLabel();
            this.SuspendLayout();
            // 
            // LabelStat
            // 
            this.LabelStat.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelStat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelStat.Location = new System.Drawing.Point(0, 86);
            this.LabelStat.Margin = new System.Windows.Forms.Padding(2);
            this.LabelStat.Name = "LabelStat";
            this.LabelStat.Size = new System.Drawing.Size(1920, 110);
            this.LabelStat.TabIndex = 0;
            this.LabelStat.TabStop = false;
            this.LabelStat.Text = "Итог";
            this.LabelStat.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelResult
            // 
            this.LabelResult.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelResult.Location = new System.Drawing.Point(0, 961);
            this.LabelResult.Margin = new System.Windows.Forms.Padding(2);
            this.LabelResult.Name = "LabelResult";
            this.LabelResult.Size = new System.Drawing.Size(1920, 118);
            this.LabelResult.TabIndex = 1;
            this.LabelResult.TabStop = false;
            this.LabelResult.Text = "10/10";
            this.LabelResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelResult.Click += new System.EventHandler(this.LabelResult_Click);
            // 
            // LabelSpeek
            // 
            this.LabelSpeek.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelSpeek.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSpeek.Location = new System.Drawing.Point(98, 985);
            this.LabelSpeek.Margin = new System.Windows.Forms.Padding(2);
            this.LabelSpeek.Name = "LabelSpeek";
            this.LabelSpeek.Size = new System.Drawing.Size(419, 37);
            this.LabelSpeek.TabIndex = 4;
            this.LabelSpeek.TabStop = false;
            this.LabelSpeek.Text = "Скажите “Новая игра”";
            this.LabelSpeek.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // LabelNewGame
            // 
            this.LabelNewGame.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelNewGame.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelNewGame.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelNewGame.Location = new System.Drawing.Point(1606, 985);
            this.LabelNewGame.Margin = new System.Windows.Forms.Padding(2);
            this.LabelNewGame.Name = "LabelNewGame";
            this.LabelNewGame.Size = new System.Drawing.Size(185, 37);
            this.LabelNewGame.TabIndex = 6;
            this.LabelNewGame.TabStop = false;
            this.LabelNewGame.Text = "Новая игра";
            this.LabelNewGame.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelNewGame.Click += new System.EventHandler(this.LabelNewGame_Click);
            // 
            // item
            // 
            this.item.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.item.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.item.Location = new System.Drawing.Point(0, 220);
            this.item.Margin = new System.Windows.Forms.Padding(2);
            this.item.Name = "item";
            this.item.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.item.Size = new System.Drawing.Size(960, 60);
            this.item.TabIndex = 7;
            this.item.TabStop = false;
            this.item.Text = "1";
            this.item.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelThing2
            // 
            this.LabelThing2.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing2.Location = new System.Drawing.Point(960, 220);
            this.LabelThing2.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing2.Name = "LabelThing2";
            this.LabelThing2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing2.Size = new System.Drawing.Size(960, 60);
            this.LabelThing2.TabIndex = 8;
            this.LabelThing2.TabStop = false;
            this.LabelThing2.Text = "2";
            this.LabelThing2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelThing2.Click += new System.EventHandler(this.LabelThing2_Click);
            // 
            // LabelThing3
            // 
            this.LabelThing3.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing3.Location = new System.Drawing.Point(0, 360);
            this.LabelThing3.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing3.Name = "LabelThing3";
            this.LabelThing3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing3.Size = new System.Drawing.Size(960, 60);
            this.LabelThing3.TabIndex = 9;
            this.LabelThing3.TabStop = false;
            this.LabelThing3.Text = "3";
            this.LabelThing3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelThing3.Click += new System.EventHandler(this.LabelThing3_Click);
            // 
            // LabelThing5
            // 
            this.LabelThing5.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing5.Location = new System.Drawing.Point(0, 500);
            this.LabelThing5.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing5.Name = "LabelThing5";
            this.LabelThing5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing5.Size = new System.Drawing.Size(960, 60);
            this.LabelThing5.TabIndex = 10;
            this.LabelThing5.TabStop = false;
            this.LabelThing5.Text = "5";
            this.LabelThing5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelThing7
            // 
            this.LabelThing7.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing7.Location = new System.Drawing.Point(0, 640);
            this.LabelThing7.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing7.Name = "LabelThing7";
            this.LabelThing7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing7.Size = new System.Drawing.Size(960, 60);
            this.LabelThing7.TabIndex = 11;
            this.LabelThing7.TabStop = false;
            this.LabelThing7.Text = "7";
            this.LabelThing7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelThing9
            // 
            this.LabelThing9.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing9.Location = new System.Drawing.Point(0, 780);
            this.LabelThing9.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing9.Name = "LabelThing9";
            this.LabelThing9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing9.Size = new System.Drawing.Size(960, 60);
            this.LabelThing9.TabIndex = 12;
            this.LabelThing9.TabStop = false;
            this.LabelThing9.Text = "9";
            this.LabelThing9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelThing4
            // 
            this.LabelThing4.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing4.Location = new System.Drawing.Point(960, 360);
            this.LabelThing4.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing4.Name = "LabelThing4";
            this.LabelThing4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing4.Size = new System.Drawing.Size(960, 60);
            this.LabelThing4.TabIndex = 13;
            this.LabelThing4.TabStop = false;
            this.LabelThing4.Text = "4";
            this.LabelThing4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelThing4.Click += new System.EventHandler(this.LabelThing4_Click);
            // 
            // LabelThing6
            // 
            this.LabelThing6.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing6.Location = new System.Drawing.Point(960, 500);
            this.LabelThing6.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing6.Name = "LabelThing6";
            this.LabelThing6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing6.Size = new System.Drawing.Size(960, 60);
            this.LabelThing6.TabIndex = 14;
            this.LabelThing6.TabStop = false;
            this.LabelThing6.Text = "6";
            this.LabelThing6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelThing8
            // 
            this.LabelThing8.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing8.Location = new System.Drawing.Point(960, 640);
            this.LabelThing8.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing8.Name = "LabelThing8";
            this.LabelThing8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing8.Size = new System.Drawing.Size(960, 60);
            this.LabelThing8.TabIndex = 15;
            this.LabelThing8.TabStop = false;
            this.LabelThing8.Text = "8";
            this.LabelThing8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelThing10
            // 
            this.LabelThing10.Font = new System.Drawing.Font("KTF Jermilov Solid", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelThing10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelThing10.Location = new System.Drawing.Point(960, 780);
            this.LabelThing10.Margin = new System.Windows.Forms.Padding(2);
            this.LabelThing10.Name = "LabelThing10";
            this.LabelThing10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelThing10.Size = new System.Drawing.Size(960, 60);
            this.LabelThing10.TabIndex = 16;
            this.LabelThing10.TabStop = false;
            this.LabelThing10.Text = "10";
            this.LabelThing10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelThing10);
            this.Controls.Add(this.LabelThing8);
            this.Controls.Add(this.LabelThing6);
            this.Controls.Add(this.LabelThing4);
            this.Controls.Add(this.LabelThing9);
            this.Controls.Add(this.LabelThing7);
            this.Controls.Add(this.LabelThing5);
            this.Controls.Add(this.LabelThing3);
            this.Controls.Add(this.LabelThing2);
            this.Controls.Add(this.item);
            this.Controls.Add(this.LabelNewGame);
            this.Controls.Add(this.LabelSpeek);
            this.Controls.Add(this.LabelResult);
            this.Controls.Add(this.LabelStat);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "StatControl";
            this.Size = new System.Drawing.Size(1920, 1080);
            this.Load += new System.EventHandler(this.StatControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StatControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private TransparentLabel LabelStat;
        private TransparentLabel LabelResult;
        private TransparentLabel LabelSpeek;
        private TransparentLabel LabelNewGame;
        private TransparentLabel item;
        private TransparentLabel LabelThing2;
        private TransparentLabel LabelThing3;
        private TransparentLabel LabelThing5;
        private TransparentLabel LabelThing7;
        private TransparentLabel LabelThing9;
        private TransparentLabel LabelThing4;
        private TransparentLabel LabelThing6;
        private TransparentLabel LabelThing8;
        private TransparentLabel LabelThing10;
    }
}

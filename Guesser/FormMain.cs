﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Speech.Recognition;

namespace Guesser
{
    public partial class FormMain : Form
    {
        ConfigMain configMain;
        ConfigContent configContent;
        int maxCountQuestions;

        Control currentControl;

        List<QuestionControl> questions;
        private Dictionary<Thing, bool> thingQuestion = new Dictionary<Thing, bool>();
        bool isFirstTry = false;
        List<Thing> things;
        int currentThing = 0;

        System.Timers.Timer timerToStart;

        static System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ru-ru");
        SpeechRecognitionEngine sreMain;
        SpeechRecognitionEngine sreQuestion;

        public FormMain()
        {
            initMainConfig();
            initContentConfig();
            initSpeechEngineMain();
            InitializeComponent();
            viewStart();
        }

        private void initMainConfig()
        {
            configMain = new ConfigMain();
            configMain.Path = @"config.xml";
            configMain.Read();

            try
            {
                maxCountQuestions = int.Parse(configMain.Data.MaxCountQuestions);
            }
            catch (Exception)
            {
                MessageBox.Show("В основном конфигурационном файле не корректно указан параметр maxCountQuestions, установлено значение по умолчанию (10)");
                maxCountQuestions = 10;
            }

        }

        private void initContentConfig()
        {
            configContent = new ConfigContent();
            configContent.Path = configMain.Data.Content;
            try
            {
                configContent.Read();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Environment.Exit(0);
            }
        }

        private void initSpeechEngineMain ()
        {
            sreMain = new SpeechRecognitionEngine(ci);
            sreMain.SetInputToDefaultAudioDevice();
            sreMain.SpeechRecognized += sre_SpeechReconized;

            Choices words = new Choices();
            words.Add(new string[] { "Начать игру", "Начать", "Выйти" });
            words.Add(new string[] { "Закончить", "продолжить" });
            words.Add(new string[] { "Пропустить" });
            words.Add(new string[] { "Новая игра" });
            GrammarBuilder grammarBuilder = new GrammarBuilder();
            grammarBuilder.Append(words);
            Grammar grammar = new Grammar(grammarBuilder);
            sreMain.LoadGrammar(grammar);
            sreMain.RecognizeAsync(RecognizeMode.Multiple);
        }

        private void Question_ClickAnswer(object sender, string name)
        {
            if (things[currentThing].Name == name)
            {
                thingQuestion.Add(things[currentThing], true);
                if (currentThing + 1 >= things.Count)
                {
                    viewFinishControl();
                }
                else
                {
                    currentThing++;
                    viewQuestionControl(questions[currentThing]);
                }
            }
            else
            {
                if (isFirstTry) viewDescriptionControl(createDescriptionControl(things[currentThing]));
                isFirstTry = true;
                (sender as QuestionControl).IsViewHelp = true;
            }
        }

        private void TimerToStart_Tick(object sender, EventArgs e)
        {
            timerToStart.Stop();
            timerToStart.Dispose();
            viewStart();
        }

        private void StartTimerToStart()
        {
            if (timerToStart != null)
            {
                timerToStart.Stop();
                timerToStart.Dispose();
            }
            timerToStart = new System.Timers.Timer();
            try
            {
                timerToStart.Interval = int.Parse(configMain.Data.TimerToStart) * 1000;
            }
            catch (Exception)
            {
                MessageBox.Show("В основном конфигурационном файле не корректно указан параметр timerToStart, установлено значение по умолчанию (10 минут)");
                timerToStart.Interval = 10 * 60 * 1000;
            }
            timerToStart.Elapsed += TimerToStart_Tick;
            timerToStart.Enabled = true;
        }

        private void viewDescriptionControl(DescriptionControl control)
        {
            StartTimerToStart();
            RemoveCurrentControl();

            control.Next += Control_Next;
            control.Exit += Control_Exit;

            currentControl = control;
            Action action = () => { this.Controls.Add(control); };
            if (this.InvokeRequired) Invoke(action);
            else action();
        }

        private void Control_Exit(object sender)
        {
            RemoveCurrentControl();
            viewStart();
        }

        private void Control_Next(object sender)
        {
            skipQuestion();
        }

        private DescriptionControl createDescriptionControl(Thing thing)
        {
            DescriptionControl control = new DescriptionControl();

            control.NameThing = thing.Name;
            control.DescriptionThing = thing.Description;
            control.PathImage = thing.DescriptionPathImage;
            control.PathBackgound = thing.DescriptionBackgroundPathImage;

            return control;
        }

        private void Question_ClickSkip(object sender, string name)
        {
            skipQuestion();
        }

        private void skipQuestion()
        {
            thingQuestion.Add(things[currentThing], false);
            if (currentThing + 1 >= things.Count)
                viewFinishControl();
            else
            {
                currentThing++;
                viewQuestionControl(questions[currentThing]);
            }
        }


        private void viewQuestionControl(QuestionControl question)
        {
            StartTimerToStart();
            isFirstTry = false;
            question.Exit += Question_ClickExit;
            question.Skip += Question_ClickSkip;
            question.Answer1 += Question_ClickAnswer;
            question.Answer2 += Question_ClickAnswer;
            question.Answer3 += Question_ClickAnswer;
            question.Answer4 += Question_ClickAnswer;

            initSpeechEngineQuestion(question);

            question.StartRedraw();

            RemoveCurrentControl();
            currentControl = question;

            Action action = () => { this.Controls.Add(question); };
            if (this.InvokeRequired) Invoke(action);
            else action();
        }

        private void initSpeechEngineQuestion(QuestionControl question)
        {
            removeSpeak();
            sreQuestion = new SpeechRecognitionEngine(ci);
            sreQuestion.SetInputToDefaultAudioDevice();
            sreQuestion.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechReconized);

            Choices words = new Choices();
            words.Add(new string[] { question.Answer1Text,
                                     question.Answer2Text,
                                     question.Answer3Text,
                                     question.Answer4Text,
                                    });

            GrammarBuilder gb = new GrammarBuilder();
            gb.Append(words);

            Grammar grammar = new Grammar(gb);
            sreQuestion.LoadGrammar(grammar);
            sreQuestion.RecognizeAsync(RecognizeMode.Multiple);
        }


        private void Question_ClickExit(object sender, string name)
        {
            RemoveCurrentControl();
            viewStart();
        }

        private void viewStart()
        {
            StartTimerToStart();
            RemoveCurrentControl();

            //сброс состояния игры
            isFirstTry = false;
            currentThing = 0;
            initQuestionList();

            StartControl start = new StartControl();

            if (configMain.Data.BackgroundStart != "") start.ImageBackground = new Bitmap(configMain.Data.BackgroundStart);

            start.Start += Start_Start;
            start.Exit += Start_Exit;
            currentControl = start;

            Action action = () => { this.Controls.Add(start); };
            if (this.InvokeRequired) Invoke(action);
            else action();
        }

        private void Start_Exit(object sender)
        {
            Close();
        }

        private void Start_Start(object sender)
        {
            RemoveCurrentControl();
            viewQuestionControl(questions[currentThing]);
        }

        private void viewStat()
        {
            StartTimerToStart();
            RemoveCurrentControl();
            StatControl stat = new StatControl();

            if (configMain.Data.BackgroundStat != "") stat.ImageBackground = new Bitmap(configMain.Data.BackgroundStat);

            stat.ThingQuestion = thingQuestion;
            stat.NewGame += Stat_NewGame;

            Action action = () => { this.Controls.Add(stat); };
            if (this.InvokeRequired) Invoke(action);
            else action();
            currentControl = stat;
        }

        private void Stat_NewGame(object sender)
        {
            viewStart();
        }

        private void viewFinishControl()
        {
            StartTimerToStart();
            RemoveCurrentControl();
            FinishControl finish = new FinishControl();

            if (configMain.Data.BackgroundFinish != "") finish.ImageBackground = new Bitmap(configMain.Data.BackgroundStat);

            finish.Stat += FinishControl_Stat;
            finish.NewGame += FinishControl_NewGame;
            finish.Scores = configMain.Data.Scores;
            finish.ThingQuestion = thingQuestion;

            currentControl = finish;
            Action action = () => { this.Controls.Add(finish); };
            if (this.InvokeRequired) Invoke(action);
            else action();
        }

        private void FinishControl_NewGame(object sender)
        {
            viewStart();
        }

        private void FinishControl_Stat(object sender)
        {
            viewStat();
        }

        private QuestionControl createQuestionControl(Thing thing)
        {
            QuestionControl question = new QuestionControl(thing.QuestionPathImage);
            string[] answers = { thing.Answer1, thing.Answer2, thing.Answer3, thing.Answer4 };
            string[] shuffled = answers.OrderBy(n => Guid.NewGuid()).ToArray();
            question.IsViewAnswer1 = true;
            question.Answer1Text = shuffled[0];
            question.IsViewAnswer2 = true;
            question.Answer2Text = shuffled[1];
            question.IsViewAnswer3 = true;
            question.Answer3Text = shuffled[2];
            question.IsViewAnswer4 = true;
            question.Answer4Text = shuffled[3];
            question.IsViewHelp = false;
            question.HelpText = thing.Help;
            question.PathBackgound = thing.QuestionBackgroundPathImage;
            question.Dock = DockStyle.Fill;

            return question;
        }


        private void initQuestionList()
        {
            thingQuestion = new Dictionary<Thing, bool>();
            currentThing = 0;
            initThingList();
            if (questions != null)
            {
                foreach (QuestionControl item in questions)
                {
                    item.Dispose();
                }
            }
            questions = new List<QuestionControl>();
            foreach (Thing item in things)
            {
                questions.Add(createQuestionControl(item));
            }
        }

        private void initThingList()
        {
            things = new List<Thing>(configContent.Things);
            List<Thing> shuffled = things.OrderBy(n => Guid.NewGuid()).ToList();
            int countElemant = maxCountQuestions;
            if (maxCountQuestions > things.Count) countElemant = things.Count;
            things.Clear();
            for (int i = 0; i < countElemant; i++) things.Add(shuffled[i]);
        }

        private void RemoveCurrentControl()
        {
            if (currentControl != null)
            {
                Action action = () => { Controls.Remove(currentControl); };
                if (this.InvokeRequired)
                {
                    Invoke(action);
                }
                else
                {
                    action();
                }
                if (currentControl is QuestionControl) (currentControl as QuestionControl).timerRedraw.Stop();
            }
        }

        void removeSpeak()
        {
            if (sreMain != null)
            {
                ThreadPool.QueueUserWorkItem(
                    delegate (object state)
                    {
                        IDisposable toDispose = state as IDisposable;
                        if (toDispose != null)
                        {
                            toDispose.Dispose();

                            GC.SuppressFinalize(this);
                        }
                    },
                    this.sreQuestion);
                this.sreQuestion = null;
            }
        }

        void sre_SpeechReconized(object sender, SpeechRecognizedEventArgs e)
        {
            double confidence;
            try
            {
                confidence = double.Parse(configMain.Data.Confidence);
            }
            catch (Exception)
            {
                MessageBox.Show("В основном конфигурационном файле не корректно указан параметр confidence, установлено значение по умолчанию (0.7)");
                confidence = 0.7;
            }
            if (e.Result.Confidence > confidence)
            {
                if (currentControl is QuestionControl && currentControl != null)
                {
                    if (e.Result.Text == (currentControl as QuestionControl).Answer1Text)
                    {   
                        (currentControl as QuestionControl).SpeekText = "Вы сказали: " + (currentControl as QuestionControl).Answer1Text;
                        (currentControl as QuestionControl).ClickAnswer1();
                        return;
                    }
                    if (e.Result.Text == (currentControl as QuestionControl).Answer2Text)
                    {
                        (currentControl as QuestionControl).SpeekText = "Вы сказали: " + (currentControl as QuestionControl).Answer2Text;
                        (currentControl as QuestionControl).ClickAnswer2();
                        return;
                    }
                    if (e.Result.Text == (currentControl as QuestionControl).Answer3Text)
                    {
                        (currentControl as QuestionControl).SpeekText = "Вы сказали: " + (currentControl as QuestionControl).Answer3Text;
                        (currentControl as QuestionControl).ClickAnswer3();
                        return;
                    }
                    if (e.Result.Text == (currentControl as QuestionControl).Answer4Text)
                    {
                        (currentControl as QuestionControl).SpeekText = "Вы сказали: " + (currentControl as QuestionControl).Answer4Text;
                        (currentControl as QuestionControl).ClickAnswer4();
                        return;
                    }

                    if (e.Result.Text == "Пропустить")
                    {
                        (currentControl as QuestionControl).ClickSkip();
                        return;
                    }

                    if (e.Result.Text == "Закончить")
                    {
                        (currentControl as QuestionControl).ClickExit();
                        return;
                    }

                }
                if (currentControl is StartControl && currentControl != null)
                {
                    if (e.Result.Text == "Начать игру" || (e.Result.Text == "Начать"))
                    {
                        (currentControl as StartControl).ClickStart();
                        return;
                    }
                    if (e.Result.Text == "Выйти")
                    {
                        if (configMain.Data.ExitSpeech == "True") (currentControl as StartControl).ClickExit();
                        return;
                    }
                }
                if (currentControl is DescriptionControl && currentControl != null)
                {
                    if (e.Result.Text == "продолжить")
                    {
                        (currentControl as DescriptionControl).ClickNext();
                        return;
                    }
                    if (e.Result.Text == "Закончить")
                    {
                        (currentControl as DescriptionControl).ClickExit();
                        return;
                    }
                }
                if (currentControl is StatControl && currentControl != null)
                {
                    if (e.Result.Text == "Новая игра")
                    {
                        (currentControl as StatControl).ClickNewGame();
                        return;
                    }
                }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guesser
{
    interface ISpeakDescriptionControl
    {
        void ClickExit();
        void ClickNext();
    }
}

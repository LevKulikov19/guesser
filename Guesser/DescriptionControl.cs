﻿using System;
using Guesser2._0;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Guesser
{
    public partial class DescriptionControl : UserControl, ISpeakDescriptionControl
    {
        public delegate void DescriptionControlEvent(object sender);
        public event DescriptionControlEvent Exit;
        public event DescriptionControlEvent Next;

        public string NameThing
        {
            get => LabelAnswer.Text;
            set { LabelAnswer.Text = value; }
        }

        public string DescriptionThing
        {
            get;
            set;
        }

        private string pathImage;
        public string PathImage
        {
            get => pathImage;
            set
            {
                pathImage = value;
                imageThing = new Bitmap(pathImage);
            }
        }

        public string PathBackgound
        {
            set
            {
                if (File.Exists(value))
                    imageBG = new Bitmap(value);
            }
        }

        Image imageThing;
        Image imageBG = ResourceMain.BackgroundQuality;
        Image imageMic = ResourceMain.Mic;
        Image imageBorder = ResourceMain.Border;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font mainFont;

        public DescriptionControl()
        {
            byte[] fontData = ResourceMain.KTFJermilov_Solid;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, ResourceMain.KTFJermilov_Solid.Length);
            AddFontMemResourceEx(fontPtr, (uint)ResourceMain.KTFJermilov_Solid.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            mainFont = new Font(fonts.Families[0], 16.0F);

            InitializeComponent();

            foreach (Control item in this.Controls)
            {
                if (item is TransparentLabel)
                {
                    float size = (item as TransparentLabel).Font.Size;
                    (item as TransparentLabel).Font = new Font(mainFont.FontFamily, size);
                }
            }
        }

        private void Draw ()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            gPaint.DrawImage(imageBG, ClientRectangle);

            Point descrioptionImagePosition = new Point(120, 300);
            Size descrioptionBox = new Size(800, 600);
            gPaint.DrawImage(imageThing, new Rectangle(descrioptionImagePosition, descrioptionBox));

            Point descrioptionTextBoxPosition = new Point(1040, 300);
            gPaint.DrawImage(imageBorder, new Rectangle(descrioptionTextBoxPosition, descrioptionBox));
            Brush brushFont = new SolidBrush(ColorTranslator.FromHtml("#F2D7B6"));
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            Point descrioptionTextPosition = new Point(descrioptionTextBoxPosition.X + descrioptionBox.Width / 2,
                                                        descrioptionTextBoxPosition.Y + descrioptionBox.Height / 2);
            Font mainDFont = new Font(mainFont.FontFamily, 22.0F);
            gPaint.DrawString(DescriptionThing, mainDFont, brushFont, descrioptionTextPosition, format);

            LabelSpeek.DrawText(gPaint, LabelSpeek.Location);
            Point pointMic = LabelSpeek.Location;
            pointMic.Offset(-55, -10);
            Rectangle rectangleMic = new Rectangle(pointMic, imageMic.Size);
            gPaint.DrawImage(imageMic, rectangleMic);

            LabelExit.DrawText(gPaint, LabelExit.Location);
            LabelAnswer.DrawText(gPaint, LabelAnswer.Location);
            LabelSkip.DrawText(gPaint, LabelSkip.Location);

            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void DescriptionControl_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void LabelExit_Click(object sender, EventArgs e)
        {
            Exit(this);
        }

        private void LabelSkip_Click(object sender, EventArgs e)
        {
            Next(this);
        }

        public void ClickExit()
        {
            LabelExit_Click(null, null);
        }

        public void ClickNext()
        {
            LabelSkip_Click(null, null);
        }

        private void DescriptionControl_Load(object sender, EventArgs e)
        {

        }
    }
}

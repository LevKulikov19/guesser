﻿
namespace Guesser
{
    partial class DescriptionControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelExit = new Guesser.TransparentLabel();
            this.LabelSpeek = new Guesser.TransparentLabel();
            this.LabelSkip = new Guesser.TransparentLabel();
            this.LabelAnswer = new Guesser.TransparentLabel();
            this.SuspendLayout();
            // 
            // LabelExit
            // 
            this.LabelExit.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelExit.Location = new System.Drawing.Point(1707, 14);
            this.LabelExit.Margin = new System.Windows.Forms.Padding(2);
            this.LabelExit.Name = "LabelExit";
            this.LabelExit.Size = new System.Drawing.Size(169, 35);
            this.LabelExit.TabIndex = 3;
            this.LabelExit.TabStop = false;
            this.LabelExit.Text = "Закончить";
            this.LabelExit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelExit.Click += new System.EventHandler(this.LabelExit_Click);
            // 
            // LabelSpeek
            // 
            this.LabelSpeek.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelSpeek.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSpeek.Location = new System.Drawing.Point(98, 985);
            this.LabelSpeek.Margin = new System.Windows.Forms.Padding(2);
            this.LabelSpeek.Name = "LabelSpeek";
            this.LabelSpeek.Size = new System.Drawing.Size(419, 34);
            this.LabelSpeek.TabIndex = 4;
            this.LabelSpeek.TabStop = false;
            this.LabelSpeek.Text = "Скажите “Продолжить”";
            this.LabelSpeek.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // LabelSkip
            // 
            this.LabelSkip.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelSkip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSkip.Location = new System.Drawing.Point(1671, 985);
            this.LabelSkip.Margin = new System.Windows.Forms.Padding(2);
            this.LabelSkip.Name = "LabelSkip";
            this.LabelSkip.Size = new System.Drawing.Size(205, 34);
            this.LabelSkip.TabIndex = 6;
            this.LabelSkip.TabStop = false;
            this.LabelSkip.Text = "Продолжить";
            this.LabelSkip.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelSkip.Click += new System.EventHandler(this.LabelSkip_Click);
            // 
            // LabelAnswer
            // 
            this.LabelAnswer.BackColor = System.Drawing.Color.Black;
            this.LabelAnswer.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelAnswer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelAnswer.Location = new System.Drawing.Point(0, 86);
            this.LabelAnswer.Margin = new System.Windows.Forms.Padding(2);
            this.LabelAnswer.Name = "LabelAnswer";
            this.LabelAnswer.Size = new System.Drawing.Size(1920, 110);
            this.LabelAnswer.TabIndex = 7;
            this.LabelAnswer.TabStop = false;
            this.LabelAnswer.Text = "Диапроектор";
            this.LabelAnswer.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DescriptionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelAnswer);
            this.Controls.Add(this.LabelSkip);
            this.Controls.Add(this.LabelSpeek);
            this.Controls.Add(this.LabelExit);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DescriptionControl";
            this.Size = new System.Drawing.Size(1920, 1080);
            this.Load += new System.EventHandler(this.DescriptionControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DescriptionControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private TransparentLabel LabelExit;
        private TransparentLabel LabelSpeek;
        private TransparentLabel LabelSkip;
        private TransparentLabel LabelAnswer;
    }
}

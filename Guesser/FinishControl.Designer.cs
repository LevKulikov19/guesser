﻿
namespace Guesser
{
    partial class FinishControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelSpeek = new Guesser.TransparentLabel();
            this.LabelNewGame = new Guesser.TransparentLabel();
            this.LabelStat = new Guesser.TransparentLabel();
            this.LabelTitle = new Guesser.TransparentLabel();
            this.LabelCaption = new Guesser.TransparentLabel();
            this.LabelResult = new Guesser.TransparentLabel();
            this.SuspendLayout();
            // 
            // LabelSpeek
            // 
            this.LabelSpeek.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelSpeek.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSpeek.Location = new System.Drawing.Point(167, 1362);
            this.LabelSpeek.Name = "LabelSpeek";
            this.LabelSpeek.Size = new System.Drawing.Size(981, 52);
            this.LabelSpeek.TabIndex = 5;
            this.LabelSpeek.TabStop = false;
            this.LabelSpeek.Text = "Скажите “Начало” или “Показать итоги”";
            this.LabelSpeek.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // LabelNewGame
            // 
            this.LabelNewGame.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelNewGame.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelNewGame.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelNewGame.Location = new System.Drawing.Point(2524, 1357);
            this.LabelNewGame.Name = "LabelNewGame";
            this.LabelNewGame.Size = new System.Drawing.Size(278, 57);
            this.LabelNewGame.TabIndex = 7;
            this.LabelNewGame.TabStop = false;
            this.LabelNewGame.Text = "Новая игра";
            this.LabelNewGame.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelNewGame.Click += new System.EventHandler(this.LabelNewGame_Click);
            // 
            // LabelStat
            // 
            this.LabelStat.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelStat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelStat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelStat.Location = new System.Drawing.Point(2402, 55);
            this.LabelStat.Name = "LabelStat";
            this.LabelStat.Size = new System.Drawing.Size(400, 57);
            this.LabelStat.TabIndex = 8;
            this.LabelStat.TabStop = false;
            this.LabelStat.Text = "Показать итоги";
            this.LabelStat.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelStat.Click += new System.EventHandler(this.LabelStat_Click);
            // 
            // LabelTitle
            // 
            this.LabelTitle.BackColor = System.Drawing.Color.Black;
            this.LabelTitle.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelTitle.Location = new System.Drawing.Point(3, 274);
            this.LabelTitle.Name = "LabelTitle";
            this.LabelTitle.Size = new System.Drawing.Size(2877, 164);
            this.LabelTitle.TabIndex = 9;
            this.LabelTitle.TabStop = false;
            this.LabelTitle.Text = "МОЛОДЕЦ!\n";
            this.LabelTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelCaption
            // 
            this.LabelCaption.BackColor = System.Drawing.Color.Black;
            this.LabelCaption.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelCaption.Location = new System.Drawing.Point(3, 569);
            this.LabelCaption.Name = "LabelCaption";
            this.LabelCaption.Size = new System.Drawing.Size(2877, 161);
            this.LabelCaption.TabIndex = 10;
            this.LabelCaption.TabStop = false;
            this.LabelCaption.Text = "У вас здорово получилось!";
            this.LabelCaption.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelResult
            // 
            this.LabelResult.BackColor = System.Drawing.Color.Black;
            this.LabelResult.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelResult.Location = new System.Drawing.Point(3, 869);
            this.LabelResult.Name = "LabelResult";
            this.LabelResult.Size = new System.Drawing.Size(2877, 164);
            this.LabelResult.TabIndex = 11;
            this.LabelResult.TabStop = false;
            this.LabelResult.Text = "Вы набрали 10/10";
            this.LabelResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FinishControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelResult);
            this.Controls.Add(this.LabelCaption);
            this.Controls.Add(this.LabelTitle);
            this.Controls.Add(this.LabelStat);
            this.Controls.Add(this.LabelNewGame);
            this.Controls.Add(this.LabelSpeek);
            this.Name = "FinishControl";
            this.Size = new System.Drawing.Size(2880, 1662);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FinishControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private TransparentLabel LabelSpeek;
        private TransparentLabel LabelNewGame;
        private TransparentLabel LabelStat;
        private TransparentLabel LabelTitle;
        private TransparentLabel LabelResult;
        private TransparentLabel LabelCaption;
    }
}

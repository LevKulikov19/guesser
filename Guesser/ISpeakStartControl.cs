﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guesser
{
    interface ISpeakStartControl
    {
        void ClickStart();
        void ClickExit();
    }
}

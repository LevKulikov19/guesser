﻿using System;
using Guesser2._0;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Guesser
{
    public partial class StatControl : UserControl, ISpeakStatControl
    {
        public delegate void StatControlEvent(object sender);
        public event StatControlEvent NewGame;

        private Dictionary<Thing, bool> thingQuestion;
        public Dictionary<Thing, bool> ThingQuestion
        {
            get => thingQuestion;
            set
            {
                thingQuestion = value;
                RefreshLableRestlt();
            }
        }

        public Image ImageBackground
        {
            get => imageBG;
            set
            {
                imageBG = value;
            }
        }

        Image imageBG = ResourceMain.BackgroundQuality;
        Image imageMic = ResourceMain.Mic;
        Image imageSuccess = ResourceMain.success;
        Image imageFail = ResourceMain.fail;

        TransparentLabel[] lablesResult = new TransparentLabel[10];

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font mainFont;

        public StatControl()
        {
            byte[] fontData = ResourceMain.KTFJermilov_Solid;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, ResourceMain.KTFJermilov_Solid.Length);
            AddFontMemResourceEx(fontPtr, (uint)ResourceMain.KTFJermilov_Solid.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            mainFont = new Font(fonts.Families[0], 16.0F);

            InitializeComponent();

            foreach (Control item in this.Controls)
            {
                if (item is TransparentLabel)
                {
                    float size = (item as TransparentLabel).Font.Size;
                    //(item as TransparentLabel).Font = new Font(mainFont.FontFamily, size);
                }
            }

            lablesResult[0] = item;
            lablesResult[1] = LabelThing2;
            lablesResult[2] = LabelThing3;
            lablesResult[3] = LabelThing4;
            lablesResult[4] = LabelThing5;
            lablesResult[5] = LabelThing6;
            lablesResult[6] = LabelThing7;
            lablesResult[7] = LabelThing8;
            lablesResult[8] = LabelThing9;
            lablesResult[9] = LabelThing10;

            foreach (TransparentLabel item in lablesResult)
            {
                item.Visible = false;
            }

        }

        private void RefreshLableRestlt()
        {

            var successAnswer = from thing in ThingQuestion 
                                where thing.Value.Equals(true)
                                select thing;
            LabelResult.Text = successAnswer.Count() + " / " + ThingQuestion.Count().ToString();

            for (int i = 0; i < ThingQuestion.Count; i++)
            {
                TransparentLabel item = lablesResult[i];
                item.Text = ThingQuestion.Keys.ToArray()[i].Name;
            }

            Refresh();
        }

        private void Draw()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gPaint.DrawImage(imageBG, ClientRectangle);

            LabelStat.DrawText(gPaint, LabelStat.Location);

            LabelResult.DrawText(gPaint, LabelResult.Location);

            LabelSpeek.DrawText(gPaint, LabelSpeek.Location);
            Point pointMic = LabelSpeek.Location;
            pointMic.Offset(-55, -10);
            Rectangle rectangleMic = new Rectangle(pointMic, imageMic.Size);
            gPaint.DrawImage(imageMic, rectangleMic);

            LabelNewGame.DrawText(gPaint, LabelNewGame.Location);

            for (int i = 0; i < ThingQuestion.Count; i++)
            {
                TransparentLabel item = lablesResult[i];

                item.DrawText(gPaint, item.Location);
                Image imageSate = ThingQuestion.Values.ToArray()[i] ? imageSuccess : imageFail;
                
                

                Point pointThing1Result = item.Location;
                pointThing1Result.Offset(item.Size.Width / 2, 0);
                Size sizeThing1Text = gPaint.MeasureString(item.Text, item.Font).ToSize();
                pointThing1Result.Offset(-sizeThing1Text.Width / 2, 0);
                pointThing1Result.Offset(-sizeThing1Text.Height / 2 - 5, sizeThing1Text.Height / 4 - 5);
                Rectangle rectangleResult = new Rectangle(pointThing1Result.X, pointThing1Result.Y, sizeThing1Text.Height / 2, sizeThing1Text.Height / 2);
                gPaint.DrawImage(imageSate, rectangleResult);
            }



            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void StatControl_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void LabelNewGame_Click(object sender, EventArgs e)
        {
            if (NewGame != null) NewGame(this);
        }

        public void ClickNewGame()
        {
            LabelNewGame_Click(null, null);
        }

        private void StatControl_Load(object sender, EventArgs e)
        {

        }

        private void LabelResult_Click(object sender, EventArgs e)
        {

        }

        private void LabelThing2_Click(object sender, EventArgs e)
        {

        }

        private void LabelThing4_Click(object sender, EventArgs e)
        {

        }

        private void LabelThing3_Click(object sender, EventArgs e)
        {

        }
    }
}

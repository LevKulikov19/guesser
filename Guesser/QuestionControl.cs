﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guesser
{
    public partial class QuestionControl : UserControl, ISpeakQuestionControl
    {
        Task taskLoadImages;

        public delegate void QuestionControlEvent(object sender, string name);
        public event QuestionControlEvent Answer1;
        public event QuestionControlEvent Answer2;
        public event QuestionControlEvent Answer3;
        public event QuestionControlEvent Answer4;
        public event QuestionControlEvent Exit;
        public event QuestionControlEvent Skip;

        public bool IsViewAnswer1 { get; set; }
        public string Answer1Text
        {
            get => LabelAnswer1.Text;
            set
            {
                LabelAnswer1.Text = value;
            }
        }

        public bool IsViewAnswer2 { get; set; }
        public string Answer2Text
        {
            get => LabelAnswer2.Text;
            set
            {
                LabelAnswer2.Text = value;
            }
        }

        public bool IsViewAnswer3 { get; set; }
        public string Answer3Text
        {
            get => LabelAnswer3.Text;
            set
            {
                LabelAnswer3.Text = value;
            }
        }

        public bool IsViewAnswer4 { get; set; }
        public string Answer4Text
        {
            get => LabelAnswer4.Text;
            set
            {
                LabelAnswer4.Text = value;
            }
        }

        public bool IsViewHelp { get; set; }
        public string HelpText {
            get => LabelHelp.Text;
            set
            {
                LabelHelp.Text = "Подсказка: " + value;
            }
        }

        public string SpeekText
        {
            get => LabelSpeek.Text;
            set
            {
                LabelSpeek.Text = value;
            }
        }

        private string pathImage;
        public string PathImage
        {
            get => pathImage;
            set
            {
                pathImage = value;
                this.taskLoadImages = new Task(()=>{
                    lock (imagesObjectLock)
                    {
                        loadImagesObject();
                        countFramesObject = imagesObject.Length;
                        currentFrameObject = 1;
                    }
                });
            }
        }

        public string PathBackgound
        {
            set
            {
                if (File.Exists(value))
                    imageBG = new Bitmap(value);
            }
        }

        public System.Timers.Timer timerRedraw;
        object imagesObjectLock = new object();
        byte[][] imagesObject;
        int currentFrameObject = 0;
        int countFramesObject = 0;
        Point objectLocation;

        Color colorDisableButton = ColorTranslator.FromHtml("#565656");


        Image imageBG = ResourceMain.BackgroundQuality;
        Image imageMic = ResourceMain.Mic;
        object imageBGLock = new object();

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font mainFont;

        public QuestionControl(string pathImages)
        {
            PathImage = pathImages;
            if (taskLoadImages != null) taskLoadImages.Start();
            byte[] fontData = ResourceMain.KTFJermilov_Solid;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, ResourceMain.KTFJermilov_Solid.Length);
            AddFontMemResourceEx(fontPtr, (uint)ResourceMain.KTFJermilov_Solid.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            mainFont = new Font(fonts.Families[0], 16.0F);

            InitializeComponent();

            foreach (Control item in this.Controls)
            {
                if (item is TransparentLabel)
                {
                    float size = (item as TransparentLabel).Font.Size;
                    (item as TransparentLabel).Font = new Font(mainFont.FontFamily, size);
                }
            }

            refreshLocation();

            timerRedraw = new System.Timers.Timer();
            timerRedraw.Interval = 40;
            timerRedraw.Elapsed += TimerRedraw_Tick;

            Draw();
        }

        public void StartRedraw()
        {

            timerRedraw.Enabled = true;

        }

        void refreshLocation()
        {
            LabelQuestion.Location = new Point(0, (int)(ClientRectangle.Height * 0.063));
            LabelQuestion.Size = new Size(ClientRectangle.Width, 70);

            LabelExit.Location = new Point(ClientRectangle.Width - 40 - LabelExit.Size.Width, 40);

            objectLocation = new Point((int)(ClientRectangle.Width * 0.1375), (int)(ClientRectangle.Height * 0.274));

        }

        private void TimerRedraw_Tick(object sender, EventArgs e)
        {
            Draw();
        }

        private void Draw()
        {
            if (IsDisposed) return;
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            refreshLocation();
            lock(imageBGLock)
            {
                gPaint.DrawImage(imageBG, ClientRectangle);

            }

            if (countFramesObject != 0 && imagesObject != null)
            {
                if (currentFrameObject > countFramesObject) currentFrameObject = 1;

                Image imageThing;
                using (var ms = new MemoryStream(imagesObject[currentFrameObject - 1]))
                {
                    imageThing = Image.FromStream(ms);
                }
                gPaint.DrawImage(imageThing, objectLocation);
                imageThing.Dispose();
                currentFrameObject++;
            }

            LabelQuestion.DrawText(gPaint, LabelQuestion.Location);

            LabelExit.DrawText(gPaint, LabelExit.Location);

            LabelSpeek.DrawText(gPaint, LabelSpeek.Location);
            Point pointMic = LabelSpeek.Location;
            pointMic.Offset(-55, -10);
            Rectangle rectangleMic = new Rectangle(pointMic, imageMic.Size);
            gPaint.DrawImage(imageMic, rectangleMic);

            if (IsViewHelp) LabelHelp.DrawText(gPaint, LabelHelp.Location);
            
            LabelSkip.DrawText(gPaint, LabelSkip.Location);

            if (IsViewAnswer1) LabelAnswer1.DrawText(gPaint, LabelAnswer1.Location);
            if (IsViewAnswer2) LabelAnswer2.DrawText(gPaint, LabelAnswer2.Location);
            if (IsViewAnswer3) LabelAnswer3.DrawText(gPaint, LabelAnswer3.Location);
            if (IsViewAnswer4) LabelAnswer4.DrawText(gPaint, LabelAnswer4.Location);

            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void QuestionControl_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void loadImagesObject ()
        {
            DirectoryInfo directory = new DirectoryInfo(PathImage);
            FileInfo[] files = directory.GetFiles();

            imagesObject = new byte[files.Length][];

            for (int i = 0; i < files.Length; i++)
            {

                imagesObject[i] = new byte[files[i].Length];

                using (BinaryReader reader = new BinaryReader(File.Open(files[i].FullName, FileMode.Open)))
                {
                    if (imagesObject != null)
                    {
                        imagesObject[i] = reader.ReadBytes((int)files[i].Length);
                    }
                }
            }
        }

        private void LabelExit_Click(object sender, EventArgs e)
        {
            if (Exit != null) Exit(this, "Exit");
        }

        private void LabelSkip_Click(object sender, EventArgs e)
        {
            if (Skip != null) Skip(this, "Skip");
        }

        public void LabelAnswer1_Click(object sender, EventArgs e)
        {
            if (LabelAnswer1.ForeColor != colorDisableButton)
            {
                LabelAnswer1.ForeColor = colorDisableButton;
                if (Answer1 != null) Answer1(this, (sender as TransparentLabel).Text);
            }
        }

        private void LabelAnswer2_Click(object sender, EventArgs e)
        {
            if (LabelAnswer2.ForeColor != colorDisableButton)
            {
                LabelAnswer2.ForeColor = colorDisableButton;
                if (Answer2 != null) Answer2(this, (sender as TransparentLabel).Text);
            }

        }

        private void LabelAnswer3_Click(object sender, EventArgs e)
        {
            if (LabelAnswer3.ForeColor != colorDisableButton)
            {
                LabelAnswer3.ForeColor = colorDisableButton;
                if (Answer3 != null) Answer3(this, (sender as TransparentLabel).Text);
            }
        }

        private void LabelAnswer4_Click(object sender, EventArgs e)
        {
            if (LabelAnswer4.ForeColor != colorDisableButton)
            {
                LabelAnswer4.ForeColor = colorDisableButton;
                if (Answer4 != null) Answer4(this, (sender as TransparentLabel).Text);
            }
        }

        private void QuestionControl_Load(object sender, EventArgs e)
        {

        }

        public new virtual void Dispose()
        {
            timerRedraw.Stop();
            timerRedraw.Dispose();
            imagesObject = null;
        }

        public void ClickAnswer1()
        {
            LabelAnswer1_Click(LabelAnswer1, null);
        }

        public void ClickAnswer2()
        {
            LabelAnswer2_Click(LabelAnswer2, null);
        }

        public void ClickAnswer3()
        {
            LabelAnswer3_Click(LabelAnswer3, null);
        }

        public void ClickAnswer4()
        {
            LabelAnswer4_Click(LabelAnswer4, null);
        }

        public void ClickExit()
        {
            LabelExit_Click(null, null);
        }

        public void ClickSkip()
        {
            LabelSkip_Click(null, null);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void LabelHelp_Click(object sender, EventArgs e)
        {

        }
    }
}

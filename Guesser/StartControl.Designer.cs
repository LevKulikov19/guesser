﻿
namespace Guesser
{
    partial class StartControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelSpeek = new Guesser.TransparentLabel();
            this.LabelExit = new Guesser.TransparentLabel();
            this.LabelStart = new Guesser.TransparentLabel();
            this.LabelLogo = new Guesser.TransparentLabel();
            this.LabelGameName = new Guesser.TransparentLabel();
            this.SuspendLayout();
            // 
            // LabelSpeek
            // 
            this.LabelSpeek.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelSpeek.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSpeek.Location = new System.Drawing.Point(98, 985);
            this.LabelSpeek.Margin = new System.Windows.Forms.Padding(2);
            this.LabelSpeek.Name = "LabelSpeek";
            this.LabelSpeek.Size = new System.Drawing.Size(419, 35);
            this.LabelSpeek.TabIndex = 4;
            this.LabelSpeek.TabStop = false;
            this.LabelSpeek.Text = "Скажите “Начать игру”";
            this.LabelSpeek.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // LabelExit
            // 
            this.LabelExit.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelExit.Location = new System.Drawing.Point(1726, 14);
            this.LabelExit.Margin = new System.Windows.Forms.Padding(2);
            this.LabelExit.Name = "LabelExit";
            this.LabelExit.Size = new System.Drawing.Size(107, 32);
            this.LabelExit.TabIndex = 3;
            this.LabelExit.TabStop = false;
            this.LabelExit.Text = "Выход";
            this.LabelExit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelExit.Click += new System.EventHandler(this.LabelExit_Click);
            // 
            // LabelStart
            // 
            this.LabelStart.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelStart.Location = new System.Drawing.Point(2, 532);
            this.LabelStart.Margin = new System.Windows.Forms.Padding(2);
            this.LabelStart.Name = "LabelStart";
            this.LabelStart.Size = new System.Drawing.Size(1920, 83);
            this.LabelStart.TabIndex = 2;
            this.LabelStart.TabStop = false;
            this.LabelStart.Text = "НАЧАТЬ ИГРУ";
            this.LabelStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LabelStart.Click += new System.EventHandler(this.LabelStart_Click);
            // 
            // LabelLogo
            // 
            this.LabelLogo.Font = new System.Drawing.Font("KTF Jermilov Solid", 84F);
            this.LabelLogo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelLogo.Location = new System.Drawing.Point(0, 427);
            this.LabelLogo.Margin = new System.Windows.Forms.Padding(2);
            this.LabelLogo.Name = "LabelLogo";
            this.LabelLogo.Size = new System.Drawing.Size(1920, 99);
            this.LabelLogo.TabIndex = 1;
            this.LabelLogo.TabStop = false;
            this.LabelLogo.Text = "?";
            this.LabelLogo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LabelLogo.Click += new System.EventHandler(this.LabelLogo_Click);
            // 
            // LabelGameName
            // 
            this.LabelGameName.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelGameName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelGameName.Location = new System.Drawing.Point(0, 316);
            this.LabelGameName.Margin = new System.Windows.Forms.Padding(2);
            this.LabelGameName.Name = "LabelGameName";
            this.LabelGameName.Size = new System.Drawing.Size(1920, 97);
            this.LabelGameName.TabIndex = 0;
            this.LabelGameName.TabStop = false;
            this.LabelGameName.Text = "Угадайка  ";
            this.LabelGameName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelGameName.Click += new System.EventHandler(this.LabelGameName_Click);
            // 
            // StartControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelSpeek);
            this.Controls.Add(this.LabelExit);
            this.Controls.Add(this.LabelStart);
            this.Controls.Add(this.LabelLogo);
            this.Controls.Add(this.LabelGameName);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "StartControl";
            this.Size = new System.Drawing.Size(1920, 1080);
            this.Load += new System.EventHandler(this.StartControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StartControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private TransparentLabel LabelGameName;
        private TransparentLabel LabelLogo;
        private TransparentLabel LabelStart;
        private TransparentLabel LabelExit;
        private TransparentLabel LabelSpeek;
    }
}

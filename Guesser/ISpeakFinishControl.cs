﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guesser
{
    interface ISpeakFinishControl
    {
        void ClickStat();
        void ClickNewGame();
    }
}

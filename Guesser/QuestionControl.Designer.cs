﻿
using System.Drawing;

namespace Guesser
{
    partial class QuestionControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelAnswer4 = new Guesser.TransparentLabel();
            this.LabelAnswer3 = new Guesser.TransparentLabel();
            this.LabelAnswer2 = new Guesser.TransparentLabel();
            this.LabelAnswer1 = new Guesser.TransparentLabel();
            this.LabelSkip = new Guesser.TransparentLabel();
            this.LabelHelp = new Guesser.TransparentLabel();
            this.LabelSpeek = new Guesser.TransparentLabel();
            this.LabelExit = new Guesser.TransparentLabel();
            this.LabelQuestion = new Guesser.TransparentLabel();
            this.SuspendLayout();
            // 
            // LabelAnswer4
            // 
            this.LabelAnswer4.Font = new System.Drawing.Font("KTF Jermilov Solid", 56.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAnswer4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelAnswer4.Location = new System.Drawing.Point(921, 786);
            this.LabelAnswer4.Name = "LabelAnswer4";
            this.LabelAnswer4.Size = new System.Drawing.Size(785, 80);
            this.LabelAnswer4.TabIndex = 9;
            this.LabelAnswer4.TabStop = false;
            this.LabelAnswer4.Text = "Аппарат Брайля";
            this.LabelAnswer4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.LabelAnswer4.Click += new System.EventHandler(this.LabelAnswer4_Click);
            // 
            // LabelAnswer3
            // 
            this.LabelAnswer3.Font = new System.Drawing.Font("KTF Jermilov Solid", 56.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAnswer3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelAnswer3.Location = new System.Drawing.Point(921, 641);
            this.LabelAnswer3.Name = "LabelAnswer3";
            this.LabelAnswer3.Size = new System.Drawing.Size(858, 99);
            this.LabelAnswer3.TabIndex = 8;
            this.LabelAnswer3.TabStop = false;
            this.LabelAnswer3.Text = "Фотоувеличитель";
            this.LabelAnswer3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.LabelAnswer3.Click += new System.EventHandler(this.LabelAnswer3_Click);
            // 
            // LabelAnswer2
            // 
            this.LabelAnswer2.Font = new System.Drawing.Font("KTF Jermilov Solid", 56.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAnswer2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelAnswer2.Location = new System.Drawing.Point(921, 496);
            this.LabelAnswer2.Name = "LabelAnswer2";
            this.LabelAnswer2.Size = new System.Drawing.Size(785, 108);
            this.LabelAnswer2.TabIndex = 7;
            this.LabelAnswer2.TabStop = false;
            this.LabelAnswer2.Text = "Кинопроектор";
            this.LabelAnswer2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.LabelAnswer2.Click += new System.EventHandler(this.LabelAnswer2_Click);
            // 
            // LabelAnswer1
            // 
            this.LabelAnswer1.Font = new System.Drawing.Font("KTF Jermilov Solid", 56.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAnswer1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelAnswer1.Location = new System.Drawing.Point(921, 351);
            this.LabelAnswer1.Name = "LabelAnswer1";
            this.LabelAnswer1.Size = new System.Drawing.Size(785, 110);
            this.LabelAnswer1.TabIndex = 6;
            this.LabelAnswer1.TabStop = false;
            this.LabelAnswer1.Text = "Диапроектор";
            this.LabelAnswer1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.LabelAnswer1.Click += new System.EventHandler(this.LabelAnswer1_Click);
            // 
            // LabelSkip
            // 
            this.LabelSkip.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelSkip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSkip.Location = new System.Drawing.Point(1671, 985);
            this.LabelSkip.Name = "LabelSkip";
            this.LabelSkip.Size = new System.Drawing.Size(201, 44);
            this.LabelSkip.TabIndex = 5;
            this.LabelSkip.TabStop = false;
            this.LabelSkip.Text = "Пропустить";
            this.LabelSkip.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelSkip.Click += new System.EventHandler(this.LabelSkip_Click);
            // 
            // LabelHelp
            // 
            this.LabelHelp.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelHelp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelHelp.Location = new System.Drawing.Point(701, 985);
            this.LabelHelp.Name = "LabelHelp";
            this.LabelHelp.Size = new System.Drawing.Size(884, 44);
            this.LabelHelp.TabIndex = 4;
            this.LabelHelp.TabStop = false;
            this.LabelHelp.Text = "Подсказка: этот предмет отображает картинки";
            this.LabelHelp.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelHelp.Visible = false;
            this.LabelHelp.Click += new System.EventHandler(this.LabelHelp_Click);
            // 
            // LabelSpeek
            // 
            this.LabelSpeek.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelSpeek.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelSpeek.Location = new System.Drawing.Point(98, 985);
            this.LabelSpeek.Name = "LabelSpeek";
            this.LabelSpeek.Size = new System.Drawing.Size(489, 40);
            this.LabelSpeek.TabIndex = 3;
            this.LabelSpeek.TabStop = false;
            this.LabelSpeek.Text = "Скажите название";
            this.LabelSpeek.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // LabelExit
            // 
            this.LabelExit.Font = new System.Drawing.Font("KTF Jermilov Solid", 24F);
            this.LabelExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.LabelExit.Location = new System.Drawing.Point(1726, 14);
            this.LabelExit.Name = "LabelExit";
            this.LabelExit.Size = new System.Drawing.Size(177, 37);
            this.LabelExit.TabIndex = 2;
            this.LabelExit.TabStop = false;
            this.LabelExit.Text = "Закончить";
            this.LabelExit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelExit.Click += new System.EventHandler(this.LabelExit_Click);
            // 
            // LabelQuestion
            // 
            this.LabelQuestion.BackColor = System.Drawing.Color.Black;
            this.LabelQuestion.Font = new System.Drawing.Font("KTF Jermilov Solid", 71.99999F);
            this.LabelQuestion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(215)))), ((int)(((byte)(182)))));
            this.LabelQuestion.Location = new System.Drawing.Point(0, 86);
            this.LabelQuestion.Name = "LabelQuestion";
            this.LabelQuestion.Size = new System.Drawing.Size(1920, 110);
            this.LabelQuestion.TabIndex = 1;
            this.LabelQuestion.TabStop = false;
            this.LabelQuestion.Text = "Как называется этот предмет?";
            this.LabelQuestion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // QuestionControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.LabelAnswer4);
            this.Controls.Add(this.LabelAnswer3);
            this.Controls.Add(this.LabelAnswer2);
            this.Controls.Add(this.LabelAnswer1);
            this.Controls.Add(this.LabelSkip);
            this.Controls.Add(this.LabelHelp);
            this.Controls.Add(this.LabelSpeek);
            this.Controls.Add(this.LabelExit);
            this.Controls.Add(this.LabelQuestion);
            this.Font = new System.Drawing.Font("KTF Jermilov Solid", 48F);
            this.Name = "QuestionControl";
            this.Size = new System.Drawing.Size(1920, 1080);
            this.Load += new System.EventHandler(this.QuestionControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.QuestionControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion
        private TransparentLabel LabelQuestion;
        private TransparentLabel LabelExit;
        private TransparentLabel LabelSpeek;
        private TransparentLabel LabelHelp;
        private TransparentLabel LabelSkip;
        private TransparentLabel LabelAnswer1;
        private TransparentLabel LabelAnswer2;
        private TransparentLabel LabelAnswer3;
        private TransparentLabel LabelAnswer4;
    }
}

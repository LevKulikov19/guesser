﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GuesserConfigurator
{
    public partial class FormMain : Form
    {
        enum ConfigType
        {
            Main,
            Content
        }

        public FormMain()
        {
            InitializeComponent();
            viewStart();
        }

        private void viewStart ()
        {
            removeCurrentView();
            StartControl start = new StartControl();
            start.Dock = DockStyle.Fill;
            Controls.Add(start);
        }

        private void viewMainConfig(string path, bool newCfg = false)
        {
            removeCurrentView();
            MainConfigControl main = new MainConfigControl();
            if (newCfg) main.newConfig(path);
            else main.Path = path; 
            main.Dock = DockStyle.Fill;
            Controls.Add(main);
        }

        private void viewContentConfig(string path, bool newCfg = false)
        {
            removeCurrentView();
            ContentConfigControl content = new ContentConfigControl();
            if (newCfg) content.newConfig(path);
            else content.Path = path;
            content.Path = path;
            content.Dock = DockStyle.Fill;
            Controls.Add(content);
        }

        private void removeCurrentView()
        {
            foreach (Control item in Controls)
            {
                if (!(item is MenuStrip))
                {
                    Controls.Remove(item);
                    item.Dispose();
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileConfig.FileName = "config.xml";
            DialogResult result = openFileConfig.ShowDialog();
            if (result == DialogResult.OK)
            {
                openConfig(openFileConfig.FileName);
            }
        }

        private void mainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileConfig.FileName = "config.xml";
            DialogResult result = saveFileConfig.ShowDialog();
            if (result == DialogResult.OK)
            {
                viewMainConfig(saveFileConfig.FileName, true);
            }
        }

        private void contentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileConfig.FileName = "config.xml";
            DialogResult result = saveFileConfig.ShowDialog();
            if (result == DialogResult.OK)
            {
                viewContentConfig(saveFileConfig.FileName, true);
            }
        }

        private void openConfig (string path)
        {
            switch (getConfigType(path))
            {
                case ConfigType.Main:
                    viewMainConfig(path);
                    break;
                case ConfigType.Content:
                    viewContentConfig(path);
                    break;
            }
            
        }

        private ConfigType getConfigType(string path)
        {

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement root = xDoc.DocumentElement;
            if (root == null) throw new Exception("Конфигурационный файл поврежден!");
            switch (root.Name)
            {
                case "content":
                    return ConfigType.Content;
                case "main":
                    return ConfigType.Main;
                default:
                    throw new Exception("Конфигурационный файл поврежден!");
            }
        }
    }
}

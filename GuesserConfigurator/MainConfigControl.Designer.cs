﻿
namespace GuesserConfigurator
{
    partial class MainConfigControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownConfidence = new System.Windows.Forms.NumericUpDown();
            this.textBoxBackgroundStatPath = new System.Windows.Forms.TextBox();
            this.textBoxBackgroundFinishPath = new System.Windows.Forms.TextBox();
            this.textBoxBackgroundStartPath = new System.Windows.Forms.TextBox();
            this.numericUpDownTimerToStart = new System.Windows.Forms.NumericUpDown();
            this.buttonBackgroundStat = new System.Windows.Forms.Button();
            this.labelBackgroundStat = new System.Windows.Forms.Label();
            this.buttonBackgroundFinish = new System.Windows.Forms.Button();
            this.labelBackgroundFinish = new System.Windows.Forms.Label();
            this.buttonBackgroundStart = new System.Windows.Forms.Button();
            this.buttonContent = new System.Windows.Forms.Button();
            this.labelContent = new System.Windows.Forms.Label();
            this.labelBackgroundStart = new System.Windows.Forms.Label();
            this.labelMaxCountQuestions = new System.Windows.Forms.Label();
            this.numericUpDownMaxCountQuestions = new System.Windows.Forms.NumericUpDown();
            this.labelTimerToStart = new System.Windows.Forms.Label();
            this.textBoxContentPath = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelConfidence = new System.Windows.Forms.Label();
            this.labelExitSpeech = new System.Windows.Forms.Label();
            this.checkBoxExitSpeech = new System.Windows.Forms.CheckBox();
            this.listBoxScores = new System.Windows.Forms.ListBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxCaption = new System.Windows.Forms.TextBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelCaption = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownMin = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelMin = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownMax = new System.Windows.Forms.NumericUpDown();
            this.labelMax = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownConfidence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerToStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxCountQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMax)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 6;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel2, 8, 7);
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownConfidence, 1, 5);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxBackgroundStatPath, 4, 3);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxBackgroundFinishPath, 2, 3);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxBackgroundStartPath, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownTimerToStart, 5, 0);
            this.tableLayoutPanelMain.Controls.Add(this.buttonBackgroundStat, 5, 2);
            this.tableLayoutPanelMain.Controls.Add(this.labelBackgroundStat, 4, 2);
            this.tableLayoutPanelMain.Controls.Add(this.buttonBackgroundFinish, 3, 2);
            this.tableLayoutPanelMain.Controls.Add(this.labelBackgroundFinish, 2, 2);
            this.tableLayoutPanelMain.Controls.Add(this.buttonBackgroundStart, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.buttonContent, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelContent, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelBackgroundStart, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.labelMaxCountQuestions, 2, 0);
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownMaxCountQuestions, 3, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelTimerToStart, 4, 0);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxContentPath, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.buttonSave, 0, 7);
            this.tableLayoutPanelMain.Controls.Add(this.labelConfidence, 0, 5);
            this.tableLayoutPanelMain.Controls.Add(this.labelExitSpeech, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.checkBoxExitSpeech, 1, 6);
            this.tableLayoutPanelMain.Controls.Add(this.listBoxScores, 2, 5);
            this.tableLayoutPanelMain.Controls.Add(this.buttonAdd, 2, 7);
            this.tableLayoutPanelMain.Controls.Add(this.buttonRemove, 3, 7);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxTitle, 5, 5);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxCaption, 5, 6);
            this.tableLayoutPanelMain.Controls.Add(this.labelTitle, 4, 5);
            this.tableLayoutPanelMain.Controls.Add(this.labelCaption, 4, 6);
            this.tableLayoutPanelMain.Controls.Add(this.label1, 2, 4);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel1, 4, 7);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 35);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 8;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1145, 655);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // numericUpDownConfidence
            // 
            this.numericUpDownConfidence.DecimalPlaces = 3;
            this.numericUpDownConfidence.Dock = System.Windows.Forms.DockStyle.Top;
            this.numericUpDownConfidence.Enabled = false;
            this.numericUpDownConfidence.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownConfidence.Location = new System.Drawing.Point(193, 425);
            this.numericUpDownConfidence.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownConfidence.Name = "numericUpDownConfidence";
            this.numericUpDownConfidence.Size = new System.Drawing.Size(184, 26);
            this.numericUpDownConfidence.TabIndex = 36;
            this.numericUpDownConfidence.ValueChanged += new System.EventHandler(this.numericUpDownConfidence_ValueChanged);
            // 
            // textBoxBackgroundStatPath
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.textBoxBackgroundStatPath, 2);
            this.textBoxBackgroundStatPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBackgroundStatPath.Location = new System.Drawing.Point(763, 295);
            this.textBoxBackgroundStatPath.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.textBoxBackgroundStatPath.Multiline = true;
            this.textBoxBackgroundStatPath.Name = "textBoxBackgroundStatPath";
            this.textBoxBackgroundStatPath.Size = new System.Drawing.Size(379, 82);
            this.textBoxBackgroundStatPath.TabIndex = 33;
            this.textBoxBackgroundStatPath.TextChanged += new System.EventHandler(this.textBoxBackgroundStatPath_TextChanged);
            // 
            // textBoxBackgroundFinishPath
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.textBoxBackgroundFinishPath, 2);
            this.textBoxBackgroundFinishPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBackgroundFinishPath.Location = new System.Drawing.Point(383, 295);
            this.textBoxBackgroundFinishPath.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.textBoxBackgroundFinishPath.Multiline = true;
            this.textBoxBackgroundFinishPath.Name = "textBoxBackgroundFinishPath";
            this.textBoxBackgroundFinishPath.Size = new System.Drawing.Size(374, 82);
            this.textBoxBackgroundFinishPath.TabIndex = 32;
            this.textBoxBackgroundFinishPath.TextChanged += new System.EventHandler(this.textBoxBackgroundFinishPath_TextChanged);
            // 
            // textBoxBackgroundStartPath
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.textBoxBackgroundStartPath, 2);
            this.textBoxBackgroundStartPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBackgroundStartPath.Location = new System.Drawing.Point(3, 295);
            this.textBoxBackgroundStartPath.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.textBoxBackgroundStartPath.Multiline = true;
            this.textBoxBackgroundStartPath.Name = "textBoxBackgroundStartPath";
            this.textBoxBackgroundStartPath.Size = new System.Drawing.Size(374, 82);
            this.textBoxBackgroundStartPath.TabIndex = 31;
            this.textBoxBackgroundStartPath.TextChanged += new System.EventHandler(this.textBoxBackgroundStartPath_TextChanged);
            // 
            // numericUpDownTimerToStart
            // 
            this.numericUpDownTimerToStart.Dock = System.Windows.Forms.DockStyle.Top;
            this.numericUpDownTimerToStart.Enabled = false;
            this.numericUpDownTimerToStart.Location = new System.Drawing.Point(953, 3);
            this.numericUpDownTimerToStart.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownTimerToStart.Name = "numericUpDownTimerToStart";
            this.numericUpDownTimerToStart.Size = new System.Drawing.Size(189, 26);
            this.numericUpDownTimerToStart.TabIndex = 27;
            this.numericUpDownTimerToStart.ValueChanged += new System.EventHandler(this.numericUpDownTimerToStart_ValueChanged);
            // 
            // buttonBackgroundStat
            // 
            this.buttonBackgroundStat.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBackgroundStat.Enabled = false;
            this.buttonBackgroundStat.Location = new System.Drawing.Point(953, 199);
            this.buttonBackgroundStat.Name = "buttonBackgroundStat";
            this.buttonBackgroundStat.Size = new System.Drawing.Size(189, 32);
            this.buttonBackgroundStat.TabIndex = 22;
            this.buttonBackgroundStat.Text = "Выбрать";
            this.buttonBackgroundStat.UseVisualStyleBackColor = true;
            this.buttonBackgroundStat.Click += new System.EventHandler(this.buttonBackgroundStat_Click);
            // 
            // labelBackgroundStat
            // 
            this.labelBackgroundStat.AutoSize = true;
            this.labelBackgroundStat.Location = new System.Drawing.Point(763, 196);
            this.labelBackgroundStat.Name = "labelBackgroundStat";
            this.labelBackgroundStat.Size = new System.Drawing.Size(153, 60);
            this.labelBackgroundStat.TabIndex = 21;
            this.labelBackgroundStat.Text = "Изображение на заденм фоне на экране статистики";
            // 
            // buttonBackgroundFinish
            // 
            this.buttonBackgroundFinish.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBackgroundFinish.Enabled = false;
            this.buttonBackgroundFinish.Location = new System.Drawing.Point(573, 199);
            this.buttonBackgroundFinish.Name = "buttonBackgroundFinish";
            this.buttonBackgroundFinish.Size = new System.Drawing.Size(184, 32);
            this.buttonBackgroundFinish.TabIndex = 19;
            this.buttonBackgroundFinish.Text = "Выбрать";
            this.buttonBackgroundFinish.UseVisualStyleBackColor = true;
            this.buttonBackgroundFinish.Click += new System.EventHandler(this.buttonBackgroundFinish_Click);
            // 
            // labelBackgroundFinish
            // 
            this.labelBackgroundFinish.AutoSize = true;
            this.labelBackgroundFinish.Location = new System.Drawing.Point(383, 196);
            this.labelBackgroundFinish.Name = "labelBackgroundFinish";
            this.labelBackgroundFinish.Size = new System.Drawing.Size(156, 60);
            this.labelBackgroundFinish.TabIndex = 18;
            this.labelBackgroundFinish.Text = "Изображение на заденм фоне на финальном экране";
            // 
            // buttonBackgroundStart
            // 
            this.buttonBackgroundStart.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBackgroundStart.Enabled = false;
            this.buttonBackgroundStart.Location = new System.Drawing.Point(193, 199);
            this.buttonBackgroundStart.Name = "buttonBackgroundStart";
            this.buttonBackgroundStart.Size = new System.Drawing.Size(184, 32);
            this.buttonBackgroundStart.TabIndex = 17;
            this.buttonBackgroundStart.Text = "Выбрать";
            this.buttonBackgroundStart.UseVisualStyleBackColor = true;
            this.buttonBackgroundStart.Click += new System.EventHandler(this.buttonBackgroundStart_Click);
            // 
            // buttonContent
            // 
            this.buttonContent.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonContent.Enabled = false;
            this.buttonContent.Location = new System.Drawing.Point(193, 3);
            this.buttonContent.Name = "buttonContent";
            this.buttonContent.Size = new System.Drawing.Size(184, 32);
            this.buttonContent.TabIndex = 14;
            this.buttonContent.Text = "Выбрать";
            this.buttonContent.UseVisualStyleBackColor = true;
            this.buttonContent.Click += new System.EventHandler(this.buttonContent_Click);
            // 
            // labelContent
            // 
            this.labelContent.AutoSize = true;
            this.labelContent.Location = new System.Drawing.Point(3, 0);
            this.labelContent.Name = "labelContent";
            this.labelContent.Size = new System.Drawing.Size(124, 40);
            this.labelContent.TabIndex = 0;
            this.labelContent.Text = "Конфигурация контента";
            // 
            // labelBackgroundStart
            // 
            this.labelBackgroundStart.AutoSize = true;
            this.labelBackgroundStart.Location = new System.Drawing.Point(3, 196);
            this.labelBackgroundStart.Name = "labelBackgroundStart";
            this.labelBackgroundStart.Size = new System.Drawing.Size(150, 60);
            this.labelBackgroundStart.TabIndex = 15;
            this.labelBackgroundStart.Text = "Изображение на заденм фоне на начальном экране";
            // 
            // labelMaxCountQuestions
            // 
            this.labelMaxCountQuestions.AutoSize = true;
            this.labelMaxCountQuestions.Location = new System.Drawing.Point(383, 0);
            this.labelMaxCountQuestions.Name = "labelMaxCountQuestions";
            this.labelMaxCountQuestions.Size = new System.Drawing.Size(177, 60);
            this.labelMaxCountQuestions.TabIndex = 24;
            this.labelMaxCountQuestions.Text = "Максимальное количество вопросов в игре";
            // 
            // numericUpDownMaxCountQuestions
            // 
            this.numericUpDownMaxCountQuestions.Dock = System.Windows.Forms.DockStyle.Top;
            this.numericUpDownMaxCountQuestions.Enabled = false;
            this.numericUpDownMaxCountQuestions.Location = new System.Drawing.Point(573, 3);
            this.numericUpDownMaxCountQuestions.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMaxCountQuestions.Name = "numericUpDownMaxCountQuestions";
            this.numericUpDownMaxCountQuestions.Size = new System.Drawing.Size(184, 26);
            this.numericUpDownMaxCountQuestions.TabIndex = 25;
            this.numericUpDownMaxCountQuestions.ValueChanged += new System.EventHandler(this.numericUpDownMaxCountQuestions_ValueChanged);
            // 
            // labelTimerToStart
            // 
            this.labelTimerToStart.AutoSize = true;
            this.labelTimerToStart.Location = new System.Drawing.Point(763, 0);
            this.labelTimerToStart.Name = "labelTimerToStart";
            this.labelTimerToStart.Size = new System.Drawing.Size(184, 60);
            this.labelTimerToStart.TabIndex = 26;
            this.labelTimerToStart.Text = "Возврат к начальному при бездействии (секунды)";
            // 
            // textBoxContentPath
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.textBoxContentPath, 2);
            this.textBoxContentPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxContentPath.Location = new System.Drawing.Point(3, 99);
            this.textBoxContentPath.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.textBoxContentPath.Multiline = true;
            this.textBoxContentPath.Name = "textBoxContentPath";
            this.textBoxContentPath.Size = new System.Drawing.Size(374, 82);
            this.textBoxContentPath.TabIndex = 28;
            this.textBoxContentPath.TextChanged += new System.EventHandler(this.textBoxContentPath_TextChanged);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(3, 617);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(184, 33);
            this.buttonSave.TabIndex = 34;
            this.buttonSave.Text = "Сохранить изменения";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelConfidence
            // 
            this.labelConfidence.AutoSize = true;
            this.labelConfidence.Location = new System.Drawing.Point(3, 422);
            this.labelConfidence.Name = "labelConfidence";
            this.labelConfidence.Size = new System.Drawing.Size(166, 40);
            this.labelConfidence.TabIndex = 35;
            this.labelConfidence.Text = "Уверенность в распознанном слове\n";
            // 
            // labelExitSpeech
            // 
            this.labelExitSpeech.AutoSize = true;
            this.labelExitSpeech.Location = new System.Drawing.Point(3, 518);
            this.labelExitSpeech.Name = "labelExitSpeech";
            this.labelExitSpeech.Size = new System.Drawing.Size(150, 60);
            this.labelExitSpeech.TabIndex = 37;
            this.labelExitSpeech.Text = "Выход с помощью голосового управления";
            // 
            // checkBoxExitSpeech
            // 
            this.checkBoxExitSpeech.AutoSize = true;
            this.checkBoxExitSpeech.Enabled = false;
            this.checkBoxExitSpeech.Location = new System.Drawing.Point(193, 521);
            this.checkBoxExitSpeech.Name = "checkBoxExitSpeech";
            this.checkBoxExitSpeech.Size = new System.Drawing.Size(22, 21);
            this.checkBoxExitSpeech.TabIndex = 38;
            this.checkBoxExitSpeech.UseVisualStyleBackColor = true;
            this.checkBoxExitSpeech.CheckedChanged += new System.EventHandler(this.checkBoxExitSpeech_CheckedChanged);
            // 
            // listBoxScores
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.listBoxScores, 2);
            this.listBoxScores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxScores.Enabled = false;
            this.listBoxScores.FormattingEnabled = true;
            this.listBoxScores.ItemHeight = 20;
            this.listBoxScores.Items.AddRange(new object[] {
            "нет"});
            this.listBoxScores.Location = new System.Drawing.Point(395, 425);
            this.listBoxScores.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.listBoxScores.Name = "listBoxScores";
            this.tableLayoutPanelMain.SetRowSpan(this.listBoxScores, 2);
            this.listBoxScores.Size = new System.Drawing.Size(362, 186);
            this.listBoxScores.TabIndex = 39;
            this.listBoxScores.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Enabled = false;
            this.buttonAdd.Location = new System.Drawing.Point(395, 617);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(172, 33);
            this.buttonAdd.TabIndex = 40;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Enabled = false;
            this.buttonRemove.Location = new System.Drawing.Point(585, 617);
            this.buttonRemove.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(172, 33);
            this.buttonRemove.TabIndex = 41;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTitle.Enabled = false;
            this.textBoxTitle.Location = new System.Drawing.Point(953, 425);
            this.textBoxTitle.Multiline = true;
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(189, 90);
            this.textBoxTitle.TabIndex = 44;
            // 
            // textBoxCaption
            // 
            this.textBoxCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxCaption.Enabled = false;
            this.textBoxCaption.Location = new System.Drawing.Point(953, 521);
            this.textBoxCaption.Multiline = true;
            this.textBoxCaption.Name = "textBoxCaption";
            this.textBoxCaption.Size = new System.Drawing.Size(189, 90);
            this.textBoxCaption.TabIndex = 45;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(763, 422);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(182, 20);
            this.labelTitle.TabIndex = 46;
            this.labelTitle.Text = "Загаловок результата";
            // 
            // labelCaption
            // 
            this.labelCaption.AutoSize = true;
            this.labelCaption.Location = new System.Drawing.Point(763, 518);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(168, 20);
            this.labelCaption.TabIndex = 47;
            this.labelCaption.Text = "Подпись результата";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanelMain.SetColumnSpan(this.label1, 4);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(383, 392);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(759, 30);
            this.label1.TabIndex = 48;
            this.label1.Text = "Результаты";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDownMin
            // 
            this.numericUpDownMin.Enabled = false;
            this.numericUpDownMin.Location = new System.Drawing.Point(98, 3);
            this.numericUpDownMin.Name = "numericUpDownMin";
            this.numericUpDownMin.Size = new System.Drawing.Size(89, 26);
            this.numericUpDownMin.TabIndex = 50;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.numericUpDownMin, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelMin, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(760, 614);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(190, 41);
            this.tableLayoutPanel1.TabIndex = 51;
            // 
            // labelMin
            // 
            this.labelMin.AutoSize = true;
            this.labelMin.Location = new System.Drawing.Point(3, 0);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(88, 40);
            this.labelMin.TabIndex = 51;
            this.labelMin.Text = "Минимальное";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.numericUpDownMax, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelMax, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(950, 614);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(195, 41);
            this.tableLayoutPanel2.TabIndex = 52;
            // 
            // numericUpDownMax
            // 
            this.numericUpDownMax.Enabled = false;
            this.numericUpDownMax.Location = new System.Drawing.Point(100, 3);
            this.numericUpDownMax.Name = "numericUpDownMax";
            this.numericUpDownMax.Size = new System.Drawing.Size(92, 26);
            this.numericUpDownMax.TabIndex = 50;
            // 
            // labelMax
            // 
            this.labelMax.AutoSize = true;
            this.labelMax.Location = new System.Drawing.Point(3, 0);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(86, 40);
            this.labelMax.TabIndex = 51;
            this.labelMax.Text = "Максимальное";
            // 
            // MainConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "MainConfigControl";
            this.Padding = new System.Windows.Forms.Padding(0, 35, 0, 0);
            this.Size = new System.Drawing.Size(1145, 690);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownConfidence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerToStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxCountQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMax)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Label labelContent;
        private System.Windows.Forms.NumericUpDown numericUpDownTimerToStart;
        private System.Windows.Forms.Button buttonBackgroundStat;
        private System.Windows.Forms.Label labelBackgroundStat;
        private System.Windows.Forms.Button buttonBackgroundFinish;
        private System.Windows.Forms.Label labelBackgroundFinish;
        private System.Windows.Forms.Button buttonBackgroundStart;
        private System.Windows.Forms.Button buttonContent;
        private System.Windows.Forms.Label labelBackgroundStart;
        private System.Windows.Forms.Label labelMaxCountQuestions;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxCountQuestions;
        private System.Windows.Forms.Label labelTimerToStart;
        private System.Windows.Forms.TextBox textBoxContentPath;
        private System.Windows.Forms.TextBox textBoxBackgroundStatPath;
        private System.Windows.Forms.TextBox textBoxBackgroundFinishPath;
        private System.Windows.Forms.TextBox textBoxBackgroundStartPath;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.NumericUpDown numericUpDownConfidence;
        private System.Windows.Forms.Label labelConfidence;
        private System.Windows.Forms.Label labelExitSpeech;
        private System.Windows.Forms.CheckBox checkBoxExitSpeech;
        private System.Windows.Forms.ListBox listBoxScores;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxCaption;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown numericUpDownMax;
        private System.Windows.Forms.Label labelMax;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.NumericUpDown numericUpDownMin;
        private System.Windows.Forms.Label labelMin;
    }
}

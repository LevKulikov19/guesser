﻿using Guesser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuesserConfigurator
{
    public partial class MainConfigControl : UserControl
    {
        ConfigMain config;
        private string path;
        public string Path
        {
            get => path;
            set
            {
                if (!File.Exists(value)) throw new Exception("Файл не может быть загружен!");
                path = value;
                InitializeConfig();
            }
        }

        public MainConfigControl()
        {
            InitializeComponent();
        }

        private void InitializeConfig()
        {
            config = new ConfigMain();
            config.Path = Path;
            config.Read();
            UpdateControl();
        }

        private void UpdateControl()
        {
            if (config.Data == null)
            {
                buttonContent.Enabled = false;
                textBoxContentPath.Text = "";

                numericUpDownMaxCountQuestions.Enabled = false;
                numericUpDownMaxCountQuestions.Value = 0;

                numericUpDownTimerToStart.Enabled = false;
                numericUpDownTimerToStart.Value = 0;

                numericUpDownConfidence.Enabled = false;
                numericUpDownConfidence.Value = 0;

                checkBoxExitSpeech.Enabled = false;

                buttonBackgroundStart.Enabled = false;
                textBoxBackgroundStartPath.Text = "";

                buttonBackgroundFinish.Enabled = false;
                textBoxBackgroundFinishPath.Text = "";

                buttonBackgroundStat.Enabled = false;
                textBoxBackgroundStatPath.Text = "";

                listBoxScores.Enabled = false;
                buttonAdd.Enabled = false;
            }
            else
            {
                buttonContent.Enabled = true;
                textBoxContentPath.Text = config.Data.Content;

                numericUpDownMaxCountQuestions.Enabled = true;
                try
                {
                    numericUpDownMaxCountQuestions.Value = int.Parse(config.Data.MaxCountQuestions);
                }
                catch (Exception)
                {
                    numericUpDownMaxCountQuestions.Value = 0;
                    config.Data.MaxCountQuestions = "0";
                }

                numericUpDownTimerToStart.Enabled = true;
                try
                {
                    numericUpDownTimerToStart.Value = int.Parse(config.Data.TimerToStart);
                }
                catch (Exception)
                {
                    numericUpDownTimerToStart.Value = 0;
                    config.Data.TimerToStart = "0";
                }

                numericUpDownConfidence.Enabled = true;
                try
                {
                    numericUpDownConfidence.Value = (decimal)double.Parse(config.Data.Confidence);
                }
                catch (Exception)
                {
                    numericUpDownConfidence.Value = 0.7M;
                    config.Data.Confidence = "0,7";
                }

                checkBoxExitSpeech.Enabled = true;
                try
                {
                    checkBoxExitSpeech.Checked = bool.Parse(config.Data.ExitSpeech);
                }
                catch (Exception)
                {
                    checkBoxExitSpeech.Checked = false;
                    config.Data.ExitSpeech = "False";
                }

                buttonBackgroundStart.Enabled = true;
                textBoxBackgroundStartPath.Text = config.Data.BackgroundStart;

                buttonBackgroundFinish.Enabled = true;
                textBoxBackgroundFinishPath.Text = config.Data.BackgroundFinish;

                buttonBackgroundStat.Enabled = true;
                textBoxBackgroundStatPath.Text = config.Data.BackgroundStat;

                buttonAdd.Enabled = true;
                if (config.Data.Scores.Count != 0)
                {
                    listBoxScores.Enabled = true;
                    listBoxScores.Items.Clear();
                    foreach (Score score in config.Data.Scores)
                    {
                        listBoxScores.Items.Add(score.Title);
                    }
                }
                else
                {
                    listBoxScores.Enabled = false;
                    listBoxScores.Items.Clear();
                }
            }
        }

        private void UpdateScore()
        {
            if (listBoxScores.SelectedIndex == -1)
            {
                buttonRemove.Enabled = false;

                numericUpDownMin.Enabled = false;
                numericUpDownMax.Enabled = false;

                textBoxTitle.Enabled = false;
                textBoxCaption.Enabled = false;
            }
            else
            {
                Score score = config.Data.Scores[listBoxScores.SelectedIndex];
                buttonAdd.Enabled = true;
                buttonRemove.Enabled = true;

                numericUpDownMin.Enabled = true;
                try
                {
                    numericUpDownMin.Value = int.Parse(score.Min);
                }
                catch(Exception)
                {
                    numericUpDownMin.Value = 0;
                    score.Min = "0";
                }

                numericUpDownMax.Enabled = true;
                try
                {
                    numericUpDownMax.Value = int.Parse(score.Max);
                }
                catch (Exception)
                {
                    numericUpDownMax.Value = 0;
                    score.Max = "0";
                }

                textBoxTitle.Enabled = true;
                textBoxTitle.Text = score.Title;

                textBoxCaption.Enabled = true;
                textBoxCaption.Text = score.Caption;
            }

        }

        private void buttonContent_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = (new FileInfo(Path)).DirectoryName;
            fileDialog.Filter = "конфигурационные файлы (*.xml)|*.xml|все файлы (*.*)|*.*";
            DialogResult dialog = fileDialog.ShowDialog();
            if (dialog == DialogResult.OK)
            {
                config.Data.Content = path;
                textBoxContentPath.Text = config.Data.Content;
            }
        }

        private void numericUpDownMaxCountQuestions_ValueChanged(object sender, EventArgs e)
        {
            config.Data.MaxCountQuestions = (sender as NumericUpDown).Value.ToString();
        }

        private void numericUpDownTimerToStart_ValueChanged(object sender, EventArgs e)
        {
            config.Data.TimerToStart = (sender as NumericUpDown).Value.ToString();
        }

        private void numericUpDownConfidence_ValueChanged(object sender, EventArgs e)
        {
            config.Data.Confidence = (sender as NumericUpDown).Value.ToString();
        }

        private void checkBoxExitSpeech_CheckedChanged(object sender, EventArgs e)
        {
            config.Data.ExitSpeech = (sender as CheckBox).Checked.ToString();
        }

        private void buttonBackgroundStart_Click(object sender, EventArgs e)
        {
            string path = fileDialog();
            if (path != "")
            {
                config.Data.BackgroundStart = path;
                textBoxBackgroundStartPath.Text = config.Data.BackgroundStart;
            }
        }

        private void buttonBackgroundFinish_Click(object sender, EventArgs e)
        {
            string path = fileDialog();
            if (path != "")
            {
                config.Data.BackgroundFinish = path;
                textBoxBackgroundFinishPath.Text = config.Data.BackgroundFinish;
            }
        }

        private void buttonBackgroundStat_Click(object sender, EventArgs e)
        {
            string path = fileDialog();
            if (path != "")
            {
                config.Data.BackgroundStat = path;
                textBoxBackgroundStatPath.Text = config.Data.BackgroundStat;
            }
        }

        private string fileDialog()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = (new FileInfo(Path)).DirectoryName;
            fileDialog.Filter = "image files (*.jpeg;*.png)|*.jpeg;*.png|All files (*.*)|*.*";
            DialogResult dialog = fileDialog.ShowDialog();
            if (dialog == DialogResult.OK)
            {
                return fileDialog.FileName;
            }
            return "";
        }

        private void textBoxContentPath_TextChanged(object sender, EventArgs e)
        {
            config.Data.Content = (sender as TextBox).Text;
        }

        private void textBoxBackgroundStartPath_TextChanged(object sender, EventArgs e)
        {
            config.Data.BackgroundStart = (sender as TextBox).Text;
        }

        private void textBoxBackgroundFinishPath_TextChanged(object sender, EventArgs e)
        {
            config.Data.BackgroundFinish = (sender as TextBox).Text;
        }

        private void textBoxBackgroundStatPath_TextChanged(object sender, EventArgs e)
        {
            config.Data.BackgroundStat = (sender as TextBox).Text;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            config.Write();
            MessageBox.Show("Изменения сохранены!");
        }

        public void newConfig (string path)
        {
            this.path = path;
            FileStream fs = File.Create(Path);
            fs.Close();
            fs.Dispose();
            ConfigMain.WriteEmpty(Path);
            InitializeConfig();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateScore();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Score score = new Score();
            string baseName = "Без названия";
            int count = 0;
            foreach (string item in listBoxScores.Items)
            {
                if ((item).Contains(baseName)) count++;
            }
            baseName = baseName + " " + (count + 1).ToString();

            score.Title = baseName;
            score.Caption = "";
            score.Min = "0";
            score.Max = "0";
            config.Data.Scores.Add(score);
            UpdateControl();
            UpdateScore();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                "Вы уверены что хотите удалить \"" + config.Data.Scores[listBoxScores.SelectedIndex].Title + "\"",
                "Удаление предмета",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
                config.Data.Scores.RemoveAt(listBoxScores.SelectedIndex);
            UpdateControl();
            UpdateScore();
        }
    }
}

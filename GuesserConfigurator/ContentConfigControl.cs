﻿using Guesser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuesserConfigurator
{
    public partial class ContentConfigControl : UserControl
    {
        Thing currentThing;
        ConfigContent config;
        private string path;
        public string Path
        {
            get => path;
            set
            {
                if (!File.Exists(value)) throw new Exception("Файл не может быть загружен!");
                path = value;
                InitializeConfig();
            }
        }
        public ContentConfigControl()
        {
            InitializeComponent();
        }

        private void InitializeConfig ()
        {
            config = new ConfigContent();
            config.Path = Path;
            config.Read();

            InitializeThingsList();
            UpdateQuestion();
        }

        private void InitializeThingsList ()
        {
            listBoxThings.Items.Clear();
            foreach (Thing item in config.Things)
            {
                listBoxThings.Items.Add(item.Name);
                item.ChangeName += Item_ChangeName_Update_listBoxThings;
            }
            listBoxThings.Enabled = true;
            buttonAdd.Enabled = true;
            buttonRemove.Enabled = true;
        }

        private void Item_ChangeName_Update_listBoxThings(object sender, string oldValue, string newValue)
        {
            int counter = 0;
            foreach (string item in listBoxThings.Items)
            {
                if (item == oldValue)
                {
                    listBoxThings.Items[counter] = newValue;
                    break;
                }
                counter++;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Thing thing = new Thing();
            config.Things.Add(thing);
            string baseName = "Без названия";
            int count = 0;
            foreach (string item in listBoxThings.Items)
            {
                if ((item).Contains(baseName)) count++;
            }
            baseName = baseName + " " + (count+1).ToString();
            thing.Name = baseName;
            listBoxThings.Items.Add(baseName);
            thing.ChangeName += Item_ChangeName_Update_listBoxThings;
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listBoxThings.SelectedIndex != -1)
            {
                DialogResult result = MessageBox.Show(
                    "Вы уверены что хотите удалить \"" + config.Things[listBoxThings.SelectedIndex].Name + "\"",
                    "Удаление предмета",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    config.Things[listBoxThings.SelectedIndex].ChangeName -= Item_ChangeName_Update_listBoxThings;
                    config.Things.Remove(config.Things[listBoxThings.SelectedIndex]);
                    listBoxThings.Items.Remove(listBoxThings.SelectedItem);
                }
            }
        }

        private void UpdateQuestion ()
        {
            if (currentThing == null)
            {
                textBoxName.Enabled = false;
                textBoxName.Text = "";

                textBoxAnswer1.Enabled = false;
                textBoxAnswer1.Text = "";

                textBoxAnswer2.Enabled = false;
                textBoxAnswer2.Text = "";

                textBoxAnswer3.Enabled = false;
                textBoxAnswer3.Text = "";

                textBoxAnswer4.Enabled = false;
                textBoxAnswer4.Text = "";

                textBoxHelp.Enabled = false;
                textBoxHelp.Text = "";

                buttonBackgroundImage.Enabled = false;
                textBoxBackgroundPath.Text = "Путь: не определен";
                textBoxBackgroundPath.Enabled = false;

                buttonImage.Enabled = false;
                textBoxImagePath.Text = "Путь: не определен";
                textBoxImagePath.Enabled = false;
            }
            else
            {
                textBoxName.Enabled = true;
                textBoxName.Text = currentThing.Name;

                textBoxAnswer1.Enabled = true;
                textBoxAnswer1.Text = currentThing.Answer1;

                textBoxAnswer2.Enabled = true;
                textBoxAnswer2.Text = currentThing.Answer2;

                textBoxAnswer3.Enabled = true;
                textBoxAnswer3.Text = currentThing.Answer3;

                textBoxAnswer4.Enabled = true;
                textBoxAnswer4.Text = currentThing.Answer4;

                textBoxHelp.Enabled = true;
                textBoxHelp.Text = currentThing.Help;

                buttonBackgroundImage.Enabled = true;
                textBoxBackgroundPath.Text = currentThing.QuestionBackgroundPathImage;
                textBoxBackgroundPath.Enabled = true;

                buttonImage.Enabled = true;
                textBoxImagePath.Text = currentThing.QuestionPathImage;
                textBoxImagePath.Enabled = true;
            }

        }

        private void textBoxName_Leave(object sender, EventArgs e)
        {
            if (currentThing != null)
            {
                currentThing.Name = (sender as TextBox).Text;
                UpdateQuestion();
            }

        }

        private void textBoxAnswer1_TextChanged(object sender, EventArgs e)
        {
            if (currentThing != null)
                currentThing.Answer1 = (sender as TextBox).Text;
        }

        private void textBoxAnswer2_TextChanged(object sender, EventArgs e)
        {
            if (currentThing != null)
                currentThing.Answer2 = (sender as TextBox).Text;
        }

        private void textBoxAnswer3_TextChanged(object sender, EventArgs e)
        {
            if (currentThing != null)
                currentThing.Answer3 = (sender as TextBox).Text;
        }

        private void textBoxAnswer4_TextChanged(object sender, EventArgs e)
        {
            if (currentThing != null)
                currentThing.Answer4 = (sender as TextBox).Text;
        }

        private void textBoxHelp_TextChanged(object sender, EventArgs e)
        {
            if (currentThing != null)
                currentThing.Help = (sender as TextBox).Text;
        }

        private void buttonBackgroundImage_Click(object sender, EventArgs e)
        {
            string path = fileDialog();
            if (path != "") currentThing.QuestionBackgroundPathImage = path;
            UpdateQuestion();
        }

        private void buttonImage_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.SelectedPath = (new FileInfo(Path)).DirectoryName;
            DialogResult dialog = folderDialog.ShowDialog();
            if (dialog == DialogResult.OK)
            {
                currentThing.QuestionPathImage = folderDialog.SelectedPath;
            }
            if (dialog == DialogResult.Cancel)
            {
                currentThing.QuestionPathImage = "";
            }
            UpdateQuestion();
        }

        private void UpdateDescription()
        {
            if (currentThing == null)
            {
                textBoxDText.Enabled = false;
                textBoxDText.Text = "";

                buttonDBackgroundImage.Enabled = false;
                textBoxDBackgroundPath.Text = "";
                textBoxDBackgroundPath.Enabled = false;

                buttonDImage.Enabled = false;
                textBoxDImagePath.Text = "";
                textBoxDImagePath.Enabled = false;
            }
            else
            {
                textBoxDText.Enabled = true;
                textBoxDText.Text = currentThing.Description;

                buttonDBackgroundImage.Enabled = true;
                textBoxDBackgroundPath.Text = currentThing.DescriptionBackgroundPathImage;
                textBoxDBackgroundPath.Enabled = true;

                buttonDImage.Enabled = true;
                textBoxDImagePath.Text = currentThing.DescriptionPathImage;
                textBoxDImagePath.Enabled = true;
            }
        }

        private void buttonDBackgroundImage_Click(object sender, EventArgs e)
        {
            string path = fileDialog();
            if (path != "") currentThing.DescriptionBackgroundPathImage = path;
            UpdateDescription();
        }

        private void buttonDImage_Click(object sender, EventArgs e)
        {
            string path = fileDialog();
            if (path != "") currentThing.DescriptionPathImage = path;
            UpdateDescription();
        }

        private void listBoxThings_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxThings.SelectedIndex != -1) currentThing = config.Things[listBoxThings.SelectedIndex];
            else currentThing = null;
            UpdateQuestion();
            UpdateDescription();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            config.Write();
            MessageBox.Show("Изменения сохранены!");
        }

        private string fileDialog()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = (new FileInfo(Path)).DirectoryName;
            fileDialog.Filter = "image files (*.jpeg;*.png)|*.jpeg;*.png|All files (*.*)|*.*";
            DialogResult dialog = fileDialog.ShowDialog();
            if (dialog == DialogResult.OK)
            {
                return fileDialog.FileName;
            }
            return "";
        }

        public void newConfig(string path)
        {
            this.path = path;
            FileStream fs = File.Create(Path);
            fs.Close();
            fs.Dispose();
            ConfigMain.WriteEmpty(Path);
            InitializeConfig();
        }

        private void textBoxBackgroundPath_TextChanged(object sender, EventArgs e)
        {
            if (textBoxBackgroundPath.Text != null && currentThing != null)
                currentThing.QuestionBackgroundPathImage = textBoxBackgroundPath.Text;
        }

        private void textBoxImagePath_TextChanged(object sender, EventArgs e)
        {
            if (textBoxImagePath.Text != null && currentThing != null)
                currentThing.QuestionPathImage = textBoxImagePath.Text;
        }

        private void tableLayoutPanelD_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBoxDBackgroundPath_TextChanged(object sender, EventArgs e)
        {
            if (textBoxDBackgroundPath.Text != null && currentThing != null)
                currentThing.DescriptionBackgroundPathImage = textBoxDBackgroundPath.Text;
        }

        private void textBoxDImagePath_TextChanged(object sender, EventArgs e)
        {
            if (textBoxDImagePath.Text != null && currentThing != null)
                currentThing.DescriptionPathImage = textBoxDImagePath.Text;
        }
    }
}

﻿
namespace GuesserConfigurator
{
    partial class ContentConfigControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelQuestion = new System.Windows.Forms.TableLayoutPanel();
            this.labelQ = new System.Windows.Forms.Label();
            this.labelImage = new System.Windows.Forms.Label();
            this.buttonImage = new System.Windows.Forms.Button();
            this.labelBackground = new System.Windows.Forms.Label();
            this.labelHelp = new System.Windows.Forms.Label();
            this.labelAnswer4 = new System.Windows.Forms.Label();
            this.labelAnswer3 = new System.Windows.Forms.Label();
            this.textBoxHelp = new System.Windows.Forms.TextBox();
            this.textBoxAnswer4 = new System.Windows.Forms.TextBox();
            this.textBoxAnswer3 = new System.Windows.Forms.TextBox();
            this.textBoxAnswer2 = new System.Windows.Forms.TextBox();
            this.labelAnswer2 = new System.Windows.Forms.Label();
            this.textBoxAnswer1 = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelAnswer1 = new System.Windows.Forms.Label();
            this.buttonBackgroundImage = new System.Windows.Forms.Button();
            this.textBoxBackgroundPath = new System.Windows.Forms.TextBox();
            this.textBoxImagePath = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxThings = new System.Windows.Forms.ListBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.tableLayoutPanelD = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxDText = new System.Windows.Forms.TextBox();
            this.labelD = new System.Windows.Forms.Label();
            this.labelDText = new System.Windows.Forms.Label();
            this.labelDImage = new System.Windows.Forms.Label();
            this.buttonDImage = new System.Windows.Forms.Button();
            this.labelDBackground = new System.Windows.Forms.Label();
            this.buttonDBackgroundImage = new System.Windows.Forms.Button();
            this.textBoxDBackgroundPath = new System.Windows.Forms.TextBox();
            this.textBoxDImagePath = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelQuestion.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelD.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelQuestion, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelD, 1, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 35);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1239, 1254);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // tableLayoutPanelQuestion
            // 
            this.tableLayoutPanelQuestion.ColumnCount = 4;
            this.tableLayoutPanelQuestion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelQuestion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelQuestion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelQuestion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelQuestion.Controls.Add(this.labelQ, 0, 0);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelImage, 2, 4);
            this.tableLayoutPanelQuestion.Controls.Add(this.buttonImage, 3, 4);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelBackground, 2, 1);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelHelp, 0, 6);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelAnswer4, 0, 5);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelAnswer3, 0, 4);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxHelp, 1, 6);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxAnswer4, 1, 5);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxAnswer3, 1, 4);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxAnswer2, 1, 3);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelAnswer2, 0, 3);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxAnswer1, 1, 2);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelName, 0, 1);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxName, 1, 1);
            this.tableLayoutPanelQuestion.Controls.Add(this.labelAnswer1, 0, 2);
            this.tableLayoutPanelQuestion.Controls.Add(this.buttonBackgroundImage, 3, 1);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxBackgroundPath, 2, 2);
            this.tableLayoutPanelQuestion.Controls.Add(this.textBoxImagePath, 2, 5);
            this.tableLayoutPanelQuestion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelQuestion.Location = new System.Drawing.Point(203, 3);
            this.tableLayoutPanelQuestion.Name = "tableLayoutPanelQuestion";
            this.tableLayoutPanelQuestion.RowCount = 7;
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanelQuestion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.6632F));
            this.tableLayoutPanelQuestion.Size = new System.Drawing.Size(1033, 621);
            this.tableLayoutPanelQuestion.TabIndex = 1;
            // 
            // labelQ
            // 
            this.labelQ.AutoSize = true;
            this.tableLayoutPanelQuestion.SetColumnSpan(this.labelQ, 4);
            this.labelQ.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQ.Location = new System.Drawing.Point(0, 0);
            this.labelQ.Margin = new System.Windows.Forms.Padding(0);
            this.labelQ.Name = "labelQ";
            this.labelQ.Size = new System.Drawing.Size(1033, 20);
            this.labelQ.TabIndex = 20;
            this.labelQ.Text = "Вопрос";
            this.labelQ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelImage
            // 
            this.labelImage.AutoSize = true;
            this.labelImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelImage.Location = new System.Drawing.Point(516, 328);
            this.labelImage.Margin = new System.Windows.Forms.Padding(0);
            this.labelImage.Name = "labelImage";
            this.labelImage.Size = new System.Drawing.Size(258, 40);
            this.labelImage.TabIndex = 14;
            this.labelImage.Text = "Изображение предмета (анимация)";
            this.labelImage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonImage
            // 
            this.buttonImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonImage.Enabled = false;
            this.buttonImage.Location = new System.Drawing.Point(777, 331);
            this.buttonImage.Name = "buttonImage";
            this.buttonImage.Size = new System.Drawing.Size(253, 32);
            this.buttonImage.TabIndex = 15;
            this.buttonImage.Text = "Выбрать";
            this.buttonImage.UseVisualStyleBackColor = true;
            this.buttonImage.Click += new System.EventHandler(this.buttonImage_Click);
            // 
            // labelBackground
            // 
            this.labelBackground.AutoSize = true;
            this.labelBackground.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelBackground.Location = new System.Drawing.Point(516, 40);
            this.labelBackground.Margin = new System.Windows.Forms.Padding(0);
            this.labelBackground.Name = "labelBackground";
            this.labelBackground.Size = new System.Drawing.Size(258, 20);
            this.labelBackground.TabIndex = 12;
            this.labelBackground.Text = "Изображение на заднем фоне";
            this.labelBackground.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelHelp
            // 
            this.labelHelp.AutoSize = true;
            this.labelHelp.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHelp.Location = new System.Drawing.Point(3, 520);
            this.labelHelp.Name = "labelHelp";
            this.labelHelp.Size = new System.Drawing.Size(252, 20);
            this.labelHelp.TabIndex = 11;
            this.labelHelp.Text = "Подсказка";
            this.labelHelp.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelAnswer4
            // 
            this.labelAnswer4.AutoSize = true;
            this.labelAnswer4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAnswer4.Location = new System.Drawing.Point(3, 424);
            this.labelAnswer4.Name = "labelAnswer4";
            this.labelAnswer4.Size = new System.Drawing.Size(252, 20);
            this.labelAnswer4.TabIndex = 10;
            this.labelAnswer4.Text = "Ответ (4 вариант)";
            this.labelAnswer4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelAnswer3
            // 
            this.labelAnswer3.AutoSize = true;
            this.labelAnswer3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAnswer3.Location = new System.Drawing.Point(3, 328);
            this.labelAnswer3.Name = "labelAnswer3";
            this.labelAnswer3.Size = new System.Drawing.Size(252, 20);
            this.labelAnswer3.TabIndex = 9;
            this.labelAnswer3.Text = "Ответ (3 вариант)";
            this.labelAnswer3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxHelp
            // 
            this.textBoxHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHelp.Enabled = false;
            this.textBoxHelp.Location = new System.Drawing.Point(261, 523);
            this.textBoxHelp.Name = "textBoxHelp";
            this.textBoxHelp.Size = new System.Drawing.Size(252, 26);
            this.textBoxHelp.TabIndex = 8;
            this.textBoxHelp.TextChanged += new System.EventHandler(this.textBoxHelp_TextChanged);
            // 
            // textBoxAnswer4
            // 
            this.textBoxAnswer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAnswer4.Enabled = false;
            this.textBoxAnswer4.Location = new System.Drawing.Point(261, 427);
            this.textBoxAnswer4.Name = "textBoxAnswer4";
            this.textBoxAnswer4.Size = new System.Drawing.Size(252, 26);
            this.textBoxAnswer4.TabIndex = 7;
            this.textBoxAnswer4.TextChanged += new System.EventHandler(this.textBoxAnswer4_TextChanged);
            // 
            // textBoxAnswer3
            // 
            this.textBoxAnswer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAnswer3.Enabled = false;
            this.textBoxAnswer3.Location = new System.Drawing.Point(261, 331);
            this.textBoxAnswer3.Name = "textBoxAnswer3";
            this.textBoxAnswer3.Size = new System.Drawing.Size(252, 26);
            this.textBoxAnswer3.TabIndex = 6;
            this.textBoxAnswer3.TextChanged += new System.EventHandler(this.textBoxAnswer3_TextChanged);
            // 
            // textBoxAnswer2
            // 
            this.textBoxAnswer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAnswer2.Enabled = false;
            this.textBoxAnswer2.Location = new System.Drawing.Point(261, 235);
            this.textBoxAnswer2.Name = "textBoxAnswer2";
            this.textBoxAnswer2.Size = new System.Drawing.Size(252, 26);
            this.textBoxAnswer2.TabIndex = 5;
            this.textBoxAnswer2.TextChanged += new System.EventHandler(this.textBoxAnswer2_TextChanged);
            // 
            // labelAnswer2
            // 
            this.labelAnswer2.AutoSize = true;
            this.labelAnswer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAnswer2.Location = new System.Drawing.Point(3, 232);
            this.labelAnswer2.Name = "labelAnswer2";
            this.labelAnswer2.Size = new System.Drawing.Size(252, 20);
            this.labelAnswer2.TabIndex = 4;
            this.labelAnswer2.Text = "Ответ (2 вариант)";
            this.labelAnswer2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxAnswer1
            // 
            this.textBoxAnswer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAnswer1.Enabled = false;
            this.textBoxAnswer1.Location = new System.Drawing.Point(261, 139);
            this.textBoxAnswer1.Name = "textBoxAnswer1";
            this.textBoxAnswer1.Size = new System.Drawing.Size(252, 26);
            this.textBoxAnswer1.TabIndex = 3;
            this.textBoxAnswer1.TextChanged += new System.EventHandler(this.textBoxAnswer1_TextChanged);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelName.Location = new System.Drawing.Point(0, 40);
            this.labelName.Margin = new System.Windows.Forms.Padding(0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(258, 20);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Название предмета";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxName
            // 
            this.textBoxName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxName.Enabled = false;
            this.textBoxName.Location = new System.Drawing.Point(261, 43);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(252, 26);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.Leave += new System.EventHandler(this.textBoxName_Leave);
            // 
            // labelAnswer1
            // 
            this.labelAnswer1.AutoSize = true;
            this.labelAnswer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAnswer1.Location = new System.Drawing.Point(3, 136);
            this.labelAnswer1.Name = "labelAnswer1";
            this.labelAnswer1.Size = new System.Drawing.Size(252, 20);
            this.labelAnswer1.TabIndex = 2;
            this.labelAnswer1.Text = "Ответ (1 вариант)";
            this.labelAnswer1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonBackgroundImage
            // 
            this.buttonBackgroundImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBackgroundImage.Enabled = false;
            this.buttonBackgroundImage.Location = new System.Drawing.Point(777, 43);
            this.buttonBackgroundImage.Name = "buttonBackgroundImage";
            this.buttonBackgroundImage.Size = new System.Drawing.Size(253, 32);
            this.buttonBackgroundImage.TabIndex = 13;
            this.buttonBackgroundImage.Text = "Выбрать";
            this.buttonBackgroundImage.UseVisualStyleBackColor = true;
            this.buttonBackgroundImage.Click += new System.EventHandler(this.buttonBackgroundImage_Click);
            // 
            // textBoxBackgroundPath
            // 
            this.tableLayoutPanelQuestion.SetColumnSpan(this.textBoxBackgroundPath, 2);
            this.textBoxBackgroundPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBackgroundPath.Enabled = false;
            this.textBoxBackgroundPath.Location = new System.Drawing.Point(519, 139);
            this.textBoxBackgroundPath.Multiline = true;
            this.textBoxBackgroundPath.Name = "textBoxBackgroundPath";
            this.tableLayoutPanelQuestion.SetRowSpan(this.textBoxBackgroundPath, 2);
            this.textBoxBackgroundPath.Size = new System.Drawing.Size(511, 186);
            this.textBoxBackgroundPath.TabIndex = 23;
            this.textBoxBackgroundPath.TextChanged += new System.EventHandler(this.textBoxBackgroundPath_TextChanged);
            // 
            // textBoxImagePath
            // 
            this.tableLayoutPanelQuestion.SetColumnSpan(this.textBoxImagePath, 2);
            this.textBoxImagePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxImagePath.Enabled = false;
            this.textBoxImagePath.Location = new System.Drawing.Point(519, 427);
            this.textBoxImagePath.Multiline = true;
            this.textBoxImagePath.Name = "textBoxImagePath";
            this.tableLayoutPanelQuestion.SetRowSpan(this.textBoxImagePath, 2);
            this.textBoxImagePath.Size = new System.Drawing.Size(511, 191);
            this.textBoxImagePath.TabIndex = 24;
            this.textBoxImagePath.TextChanged += new System.EventHandler(this.textBoxImagePath_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.listBoxThings);
            this.flowLayoutPanel1.Controls.Add(this.buttonAdd);
            this.flowLayoutPanel1.Controls.Add(this.buttonRemove);
            this.flowLayoutPanel1.Controls.Add(this.buttonSave);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.tableLayoutPanelMain.SetRowSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 1254);
            this.flowLayoutPanel1.TabIndex = 2;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Предметы";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // listBoxThings
            // 
            this.listBoxThings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxThings.Enabled = false;
            this.listBoxThings.FormattingEnabled = true;
            this.listBoxThings.ItemHeight = 20;
            this.listBoxThings.Items.AddRange(new object[] {
            "(нет)"});
            this.listBoxThings.Location = new System.Drawing.Point(3, 20);
            this.listBoxThings.Margin = new System.Windows.Forms.Padding(0);
            this.listBoxThings.Name = "listBoxThings";
            this.listBoxThings.Size = new System.Drawing.Size(200, 404);
            this.listBoxThings.TabIndex = 1;
            this.listBoxThings.SelectedIndexChanged += new System.EventHandler(this.listBoxThings_SelectedIndexChanged);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonAdd.Enabled = false;
            this.buttonAdd.Location = new System.Drawing.Point(3, 427);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(197, 32);
            this.buttonAdd.TabIndex = 2;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonRemove.Enabled = false;
            this.buttonRemove.Location = new System.Drawing.Point(3, 465);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(197, 32);
            this.buttonRemove.TabIndex = 3;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(3, 503);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(197, 32);
            this.buttonSave.TabIndex = 5;
            this.buttonSave.Text = "Сохранить изменения";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // tableLayoutPanelD
            // 
            this.tableLayoutPanelD.ColumnCount = 4;
            this.tableLayoutPanelD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelD.Controls.Add(this.textBoxDText, 1, 3);
            this.tableLayoutPanelD.Controls.Add(this.labelD, 0, 0);
            this.tableLayoutPanelD.Controls.Add(this.labelDText, 0, 3);
            this.tableLayoutPanelD.Controls.Add(this.labelDImage, 2, 1);
            this.tableLayoutPanelD.Controls.Add(this.buttonDImage, 3, 1);
            this.tableLayoutPanelD.Controls.Add(this.labelDBackground, 0, 1);
            this.tableLayoutPanelD.Controls.Add(this.buttonDBackgroundImage, 1, 1);
            this.tableLayoutPanelD.Controls.Add(this.textBoxDBackgroundPath, 0, 2);
            this.tableLayoutPanelD.Controls.Add(this.textBoxDImagePath, 2, 2);
            this.tableLayoutPanelD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelD.Location = new System.Drawing.Point(203, 630);
            this.tableLayoutPanelD.Name = "tableLayoutPanelD";
            this.tableLayoutPanelD.RowCount = 4;
            this.tableLayoutPanelD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelD.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelD.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelD.Size = new System.Drawing.Size(1033, 621);
            this.tableLayoutPanelD.TabIndex = 3;
            this.tableLayoutPanelD.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanelD_Paint);
            // 
            // textBoxDText
            // 
            this.tableLayoutPanelD.SetColumnSpan(this.textBoxDText, 3);
            this.textBoxDText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDText.Enabled = false;
            this.textBoxDText.Location = new System.Drawing.Point(261, 160);
            this.textBoxDText.Multiline = true;
            this.textBoxDText.Name = "textBoxDText";
            this.textBoxDText.Size = new System.Drawing.Size(769, 495);
            this.textBoxDText.TabIndex = 20;
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.tableLayoutPanelD.SetColumnSpan(this.labelD, 4);
            this.labelD.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelD.Location = new System.Drawing.Point(0, 0);
            this.labelD.Margin = new System.Windows.Forms.Padding(0);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(1033, 20);
            this.labelD.TabIndex = 19;
            this.labelD.Text = "Описание предмета";
            this.labelD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDText
            // 
            this.labelDText.AutoSize = true;
            this.labelDText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDText.Location = new System.Drawing.Point(0, 157);
            this.labelDText.Margin = new System.Windows.Forms.Padding(0);
            this.labelDText.Name = "labelDText";
            this.labelDText.Size = new System.Drawing.Size(258, 20);
            this.labelDText.TabIndex = 18;
            this.labelDText.Text = "Текст";
            this.labelDText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDImage
            // 
            this.labelDImage.AutoSize = true;
            this.labelDImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDImage.Location = new System.Drawing.Point(516, 40);
            this.labelDImage.Margin = new System.Windows.Forms.Padding(0);
            this.labelDImage.Name = "labelDImage";
            this.labelDImage.Size = new System.Drawing.Size(258, 20);
            this.labelDImage.TabIndex = 16;
            this.labelDImage.Text = "Изображение предмета";
            this.labelDImage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonDImage
            // 
            this.buttonDImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonDImage.Enabled = false;
            this.buttonDImage.Location = new System.Drawing.Point(777, 43);
            this.buttonDImage.Name = "buttonDImage";
            this.buttonDImage.Size = new System.Drawing.Size(253, 32);
            this.buttonDImage.TabIndex = 17;
            this.buttonDImage.Text = "Выбрать";
            this.buttonDImage.UseVisualStyleBackColor = true;
            this.buttonDImage.Click += new System.EventHandler(this.buttonDImage_Click);
            // 
            // labelDBackground
            // 
            this.labelDBackground.AutoSize = true;
            this.labelDBackground.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDBackground.Location = new System.Drawing.Point(0, 40);
            this.labelDBackground.Margin = new System.Windows.Forms.Padding(0);
            this.labelDBackground.Name = "labelDBackground";
            this.labelDBackground.Size = new System.Drawing.Size(258, 20);
            this.labelDBackground.TabIndex = 14;
            this.labelDBackground.Text = "Изображение на заднем фоне";
            this.labelDBackground.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonDBackgroundImage
            // 
            this.buttonDBackgroundImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonDBackgroundImage.Enabled = false;
            this.buttonDBackgroundImage.Location = new System.Drawing.Point(261, 43);
            this.buttonDBackgroundImage.Name = "buttonDBackgroundImage";
            this.buttonDBackgroundImage.Size = new System.Drawing.Size(252, 32);
            this.buttonDBackgroundImage.TabIndex = 15;
            this.buttonDBackgroundImage.Text = "Выбрать";
            this.buttonDBackgroundImage.UseVisualStyleBackColor = true;
            this.buttonDBackgroundImage.Click += new System.EventHandler(this.buttonDBackgroundImage_Click);
            // 
            // textBoxDBackgroundPath
            // 
            this.tableLayoutPanelD.SetColumnSpan(this.textBoxDBackgroundPath, 2);
            this.textBoxDBackgroundPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDBackgroundPath.Enabled = false;
            this.textBoxDBackgroundPath.Location = new System.Drawing.Point(3, 83);
            this.textBoxDBackgroundPath.Multiline = true;
            this.textBoxDBackgroundPath.Name = "textBoxDBackgroundPath";
            this.textBoxDBackgroundPath.Size = new System.Drawing.Size(510, 71);
            this.textBoxDBackgroundPath.TabIndex = 23;
            this.textBoxDBackgroundPath.TextChanged += new System.EventHandler(this.textBoxDBackgroundPath_TextChanged);
            // 
            // textBoxDImagePath
            // 
            this.tableLayoutPanelD.SetColumnSpan(this.textBoxDImagePath, 2);
            this.textBoxDImagePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDImagePath.Enabled = false;
            this.textBoxDImagePath.Location = new System.Drawing.Point(519, 83);
            this.textBoxDImagePath.Multiline = true;
            this.textBoxDImagePath.Name = "textBoxDImagePath";
            this.textBoxDImagePath.Size = new System.Drawing.Size(511, 71);
            this.textBoxDImagePath.TabIndex = 24;
            this.textBoxDImagePath.TextChanged += new System.EventHandler(this.textBoxDImagePath_TextChanged);
            // 
            // ContentConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ContentConfigControl";
            this.Padding = new System.Windows.Forms.Padding(0, 35, 0, 0);
            this.Size = new System.Drawing.Size(1239, 1289);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelQuestion.ResumeLayout(false);
            this.tableLayoutPanelQuestion.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanelD.ResumeLayout(false);
            this.tableLayoutPanelD.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelQuestion;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ListBox listBoxThings;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAnswer1;
        private System.Windows.Forms.Label labelAnswer1;
        private System.Windows.Forms.TextBox textBoxHelp;
        private System.Windows.Forms.TextBox textBoxAnswer4;
        private System.Windows.Forms.TextBox textBoxAnswer3;
        private System.Windows.Forms.TextBox textBoxAnswer2;
        private System.Windows.Forms.Label labelAnswer2;
        private System.Windows.Forms.Label labelHelp;
        private System.Windows.Forms.Label labelAnswer4;
        private System.Windows.Forms.Label labelAnswer3;
        private System.Windows.Forms.Label labelImage;
        private System.Windows.Forms.Button buttonImage;
        private System.Windows.Forms.Label labelBackground;
        private System.Windows.Forms.Button buttonBackgroundImage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelD;
        private System.Windows.Forms.Label labelDImage;
        private System.Windows.Forms.Button buttonDImage;
        private System.Windows.Forms.Label labelDBackground;
        private System.Windows.Forms.Button buttonDBackgroundImage;
        private System.Windows.Forms.Label labelQ;
        private System.Windows.Forms.Label labelD;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.TextBox textBoxDText;
        private System.Windows.Forms.Label labelDText;
        private System.Windows.Forms.TextBox textBoxBackgroundPath;
        private System.Windows.Forms.TextBox textBoxImagePath;
        private System.Windows.Forms.TextBox textBoxDBackgroundPath;
        private System.Windows.Forms.TextBox textBoxDImagePath;
    }
}
